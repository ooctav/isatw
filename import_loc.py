from usv_loc.models import Sala

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 001"
sala.destinatie="Laborator Acţionări Electrice   Laborator Convertoare Statice"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 002"
sala.destinatie="Laborator Maşini Electrice   Laborator Masini Electrice Speciale                               Laborator Energetica Generala si Conversia Energiei"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 003"
sala.destinatie="Laborator Materiale Electrotehnice   Laborator  Inventică si Design"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 004"
sala.destinatie="Laborator Roboţi Industrial"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 005"
sala.destinatie="Laborator Sisteme de Izolatie     Laborator Supratensiuni si Coordonarea Izolatiei"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Parter"
sala.nume="C 006"
sala.destinatie="Laborator Sisteme de Fabricatie"
sala.capacitate=10
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 101"
sala.destinatie="Laborator Ingineria Sistemelor"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 102"
sala.destinatie="Laborator Utilizarea Energiei  Electrice                             Laborator Tractiune Electrica"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 103"
sala.destinatie="Laborator Transportul si Distributia Energiei Electrice              Laborator Automatizari si Protectii prin Relee                           Laborator Tehnologie de Ramura"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 104"
sala.destinatie="Laborator Instrumentatie de Masura, Laborator Traductoare, Interfete si Achizitii de Date, Laborator Regim Deformant în Sistemele Electrice"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 105"
sala.destinatie="Laborator Metode Numerice      Laborator Simularea Circuitelor Electrice                                Laborator Proiectare Asistata in Sisteme Numerice          "
sala.capacitate=8
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 106"
sala.destinatie="Laborator Programare Asistată    Laborator Proiecte Economice"
sala.capacitate=6
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 107"
sala.destinatie="Laborator Aparate Electrice Statii si Posturi de Transformare"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 108"
sala.destinatie="Laborator Electrotehnică"
sala.capacitate=4
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj I"
sala.nume="C 109"
sala.destinatie="Laborator Sisteme de Identificare"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 201"
sala.destinatie="Laborator Electronica Analogica  Laborator Televiziune             Laborator Domotica"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 202"
sala.destinatie="Laborator Ingineria Programării   Laborator Inteligenta Artificiala"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 203"
sala.destinatie="Laborator Recunoaşterea Formelor     Laborator Prelucrarea Numerica a Imaginilor"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 204"
sala.destinatie="Laborator Structuri de Date si Tehnici de Programare      Laborator Programare Orientata pe Obiecte"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 205"
sala.destinatie="Birou cadre didactice"
sala.capacitate=3
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 206"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 207"
sala.destinatie="Laborator Programare Calculatoare"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume="C 208"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etaj II"
sala.nume=" C 209"
sala.destinatie="Laborator Baze de Date            Laborator Sisteme de Programare Pentru Management"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 301"
sala.destinatie="Laborator Elem. de  Automatizări Discrete, Modelare, Identificare, Simulare "
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 302"
sala.destinatie="Laborator Electonică de Putere  Laborator Tehnologie Electronica"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 303"
sala.destinatie="Laborator Calculatoare Numerice Laborator Sisteme cu Microprocesoare                Laborator Sisteme Inglobate"
sala.capacitate=20
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 304"
sala.destinatie="Laborator Reţele de Calculatoare  Laborator Proiectare VLSI"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 305"
sala.destinatie="Laborator Structuri Numerice"
sala.capacitate=18
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 306"
sala.destinatie="Laborator Proiectare Asistată de Calculator                              Laborator Comunicatii Analogice si Numerice"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 307"
sala.destinatie="Magazie, atelier"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal III"
sala.nume="C 308"
sala.destinatie="Laborator"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 401 a"
sala.destinatie="Doctoranzi"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 401 b"
sala.destinatie="Laborator MANSID,           resp. Gaitan Vasile"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 401 d"
sala.destinatie="Laborator MANSID (MIntViz),                                  resp. Radu Vatavu"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 401 c"
sala.destinatie="Cadre didactice"
sala.capacitate=6
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 402"
sala.destinatie="Birou cadre didactice"
sala.capacitate=0
sala.tip="birou "
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 403"
sala.destinatie="Spatiu Server"
sala.capacitate=0
sala.tip="Spatiu tehnic"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 404"
sala.destinatie="Laborator Ingineria Comunicatiilor"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 405"
sala.destinatie="Laborator cercetare (MANSID)"
sala.capacitate=15
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 407"
sala.destinatie="Sala conferinte"
sala.capacitate=25
sala.tip="aux"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 408"
sala.destinatie="Birou cadre didactice"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp C"
sala.nivel="Etal IV"
sala.nume="C 409"
sala.destinatie="Birou cadre didactice"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 001"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 002"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 003"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 004"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 005"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 007"
sala.destinatie="Inventică"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 008"
sala.destinatie="Laborator Fizică"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 009"
sala.destinatie="Laborator Electrotehnică"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 010"
sala.destinatie="Laborator Limbi Straine si Comunicare"
sala.capacitate=35
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 011"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 012"
sala.destinatie="Centru cercetare doctoranzi"
sala.capacitate=0
sala.tip="aux"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Parter"
sala.nume="D 014"
sala.destinatie="Laborator de calcul inalta performanta"
sala.capacitate=0
sala.tip="aux"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 113"
sala.destinatie="Laborator Optoelectronică si Microunde"
sala.capacitate=30
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 101"
sala.destinatie="Sală Consiliu"
sala.capacitate=26
sala.tip="curs"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 102"
sala.destinatie="Birou cadre didactice"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 103"
sala.destinatie="Arhiva"
sala.capacitate=0
sala.tip="aux"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 104 a"
sala.destinatie=" Decanat"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 104 b"
sala.destinatie="Decanat/Secretar sef facultate"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 104 c"
sala.destinatie="Decanat"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 105"
sala.destinatie="Secretariat II"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 106"
sala.destinatie="Sală seminar"
sala.capacitate=30
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 107"
sala.destinatie="Sală seminar"
sala.capacitate=30
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 108"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 109"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 110"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj I"
sala.nume="D 111"
sala.destinatie="Birou cadre didactice "
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj II"
sala.nume="D 201"
sala.destinatie="Amfiteatru Dimitrie Leonida"
sala.capacitate=88
sala.tip="curs"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj II"
sala.nume="D 202"
sala.destinatie="Amfiteatru"
sala.capacitate=99
sala.tip="curs"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj II"
sala.nume="D 203"
sala.destinatie="Amfiteatru"
sala.capacitate=99
sala.tip="curs"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj II"
sala.nume="D 204"
sala.destinatie="Amfiteatru"
sala.capacitate=99
sala.tip="curs"
sala.save()

sala = Sala()
sala.corp="Corp D"
sala.nivel="Etaj II"
sala.nume="D 205"
sala.destinatie="Laborator DSP                    Laborator Arhitecturi Paralele"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp G"
sala.nivel="Parter"
sala.nume="G 001"
sala.destinatie="Laborator de Cercetare"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp G"
sala.nivel="Etaj I"
sala.nume="G 101"
sala.destinatie="Laborator Laborator de Cercetare Sediu centru de cercetare SISCON"
sala.capacitate=12
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp G"
sala.nivel="Etaj I"
sala.nume="G 102"
sala.destinatie="Birou cadre didactice"
sala.capacitate=0
sala.tip="birou"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 1"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 2"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 3"
sala.destinatie="Laborator - NANOINF"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 5"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 6"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 7"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Parter"
sala.nume="S 8"
sala.destinatie="Laborator NANO MAT"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Etaj I"
sala.nume="103-1"
sala.destinatie="Laborator"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Etaj I"
sala.nume="104-1"
sala.destinatie="Laborator"
sala.capacitate=0
sala.tip="lab"
sala.save()

sala = Sala()
sala.corp="Corp N"
sala.nivel="Etaj I"
sala.nume="108"
sala.destinatie="Laborator"
sala.capacitate=0
sala.tip="lab"
sala.save()

