from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.template.context import RequestContext


from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator

from .forms import UsvUserForm, UserForm
from .models import UsvUser
from django.contrib.auth.models import User

@login_required
def index(request):
    """View pentru afisarea profilului utilizatorului.
    TODO: pentru superuseri (locali), permite de asemenea si schimbare de parola.
    """
    return render(request, 'home.html')


@login_required
@staff_member_required
def listare(request):
    """View pentru afisarea profilelor cadrelor didactice.
    """
    context = {
        'profile': User.objects.order_by('last_name').all()
    }

    return render(request, 'listare.html', context)

@login_required
@staff_member_required
def adaugare(request, username=None):
    """View pentru adaugare utilizatori. foloseste un ModelForm pentru UsvUser
    si creeaza si sub-form atasat pentru modelul User din Django.
    """
    context = {
        'forms': None
    }

    form = None
    usvform = None
    user = None
    usvuser = None
    if username:
        try:
            # hack, utilizatorul admin initial nu are UsvUser asociat
            user = User.objects.get(username=username)
            usvuser = user.usvuser
        except User.usvuser.RelatedObjectDoesNotExist:
            usvuser = UsvUser()

    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        usvform = UsvUserForm(request.POST, instance=usvuser)

        if form.is_valid() and usvform.is_valid():
            # validam ambele formulare independent; salvam ambele formulare,
            # fara a face commit tranzactiei in DB, dupa care adaugam obiectul
            # extra, UsvForm, in modelul parinte (user). salvam ulterior
            # rezultatul
            user = form.save()
            usvuser = usvform.save(commit=False)
            usvuser.user = user

            # toti utilizatorii adaugati sunt externi
            usvuser.is_extern = True

            usvuser.save()

            return redirect(reverse_lazy('usv_auth:listare'))
    else:
        form = UserForm(instance=user)
        usvform = UsvUserForm(instance=usvuser)

    context['forms'] = [form, usvform];
    context['edit'] = username is not None
    return render(request, 'adaugare.html', context)


@login_required
@staff_member_required
def stergere(request, username=None):
    user = User.objects.get(username=username)
    if request.user.username == user.username:
        raise ValueError('Request invalid, nu se poate șterge utilizatorul curent')

    user.delete()

    return redirect('usv_auth:listare')
