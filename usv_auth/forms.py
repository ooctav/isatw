from .models import UsvUser
from django.contrib.auth.models import User
from django.forms import ModelForm, ValidationError
from django.forms.models import inlineformset_factory
from django.core.validators import validate_email

class UsvUserForm(ModelForm):
    class Meta:
        model = UsvUser
        fields = ['titlu', 'adresa', 'telefon', 'homepage', 'fotografie']


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name',
                  'is_staff']

    """Constructor de subclasare

    Subclaseaza clasa ModelForm si in cazul in care exista o instanta valida
    (cazul in care obiectul este editat), salveaza numele utilizatorului editat
    pentru a evita conflicte de unicitate pe adresa de email.
    """
    def __init__(self, *args, **kwargs):
        self.username = None
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.username = kwargs['instance'].username

        super().__init__(*args, **kwargs)

    """Valideaza email
    
    Validator de email pentru formularul de tip User. Face campul obligatoriu
    si foloseste validatorul standard. Validarea se face in formular si nu in
    model deoarece folosim modelul standard Django.

    Tot aici se verifica daca emailul a mai fost folosit (case insensitive).

    Returns:
        None

    Raises:
        ValidationError: eroare ce va fi afisata la utilizator
    """
    def clean_email(self):
        data = self.cleaned_data['email']
        if not data or validate_email(data):
            raise ValidationError('Introduceți un email valid.')

        qs = User.objects.filter(email__iexact=data)
        if self.username:
            # excludem utilizatorul curent, daca s-a pasat o instanta (mod
            # edit)
            qs = qs.exclude(username=self.username)

        if qs.count() > 0:
            raise ValidationError('Un utilizator cu acest email există deja.')

        return data

    """Valideaza campuri obligatorii
    
    Validator de campuri obligatorii din formularul de tip User.
    Validarea se face in formular si nu in model deoarece folosim modelul
    standard Django.

    Returns:
        cleaned_data: tipul de date standard pentru input sanitizat si validat
    """
    def clean(self):
        cleaned_data = super().clean()

        for required_field in ['first_name', 'last_name']:
            data = cleaned_data.get(required_field)
            if len(data) < 1:
                self.add_error(required_field, "Câmpul este obligatoriu")

        return cleaned_data
