"""Modul de extensie al autentificariii

Foloseste obiectul de tip UsvUser pentru a extinde functionalitatea standard a
stackului Django pentru autentificare. Permite adaugarea unor campuri relevante
pentru cadre didactice (sau de tip persoana), cum ar fi titlu, imagine, precum
si alte detalii.

In arhitectura curenta, fiecare obiect de tip utilizator are si atribute
extinse, ce ii permit sa fie profesor.  Autentificarea se face de catre Django
folosind Google OAuth2.

Components:
    * ``usv_auth.models`` : modelul extins de autentificare, folosit pentru persistenta in baza de date

    * ``usv_auth.urls`` si ``usv_auth.views`` deschid accessul routerului Django catre componentele interne (modele si formulare)

    * ``usv_auth.forms``: formulare folosite in interactiunea cu utilizatorul

"""
