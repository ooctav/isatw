from django.urls import path

from . import views

app_name = 'usv_auth'

urlpatterns = [
    path('', views.index, name='profil'),
    path('adaugare', views.adaugare, name='adaugare'),
    path('editare/<str:username>', views.adaugare, name='editare'),
    path('stergere/<str:username>', views.stergere, name='stergere'),
    path('listare', views.listare, name='listare')
]
