from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'usv_auth'
    verbose_name = "Stack autentificare Orar USV"
