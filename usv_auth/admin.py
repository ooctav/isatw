from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group 

from usv_auth.models import UsvUser


class UsvUserInline(admin.StackedInline):
    """Implementeaza campurile suplimentare din modelul usv-auth in panoul de
    administrare Django.
    Se definesc explicit atributele modelului, pentru a putea folosi si
    rezultatul functiei *foto_tag*, pe care il facem read-only
    """
    model = UsvUser

    fields = ('titlu', 'adresa', 'telefon', 'homepage', 'fotografie',
              'foto_tag', 'is_extern')
    readonly_fields = ('foto_tag',)

    can_delete = False
    verbose_name_plural = 'utilizator USV'
    verbose_name = 'utilizator USV'


class UserAdmin(BaseUserAdmin):
    readonly_fields = ('date_joined', 'last_login',)
    fieldsets = (
        ('Informatii Standard', {'fields': ('username', 'first_name', 'last_name',
                                               'email', 'password', 'last_login',
                                               'date_joined')}),
    )
    inlines = (UsvUserInline,)


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(User, UserAdmin)
