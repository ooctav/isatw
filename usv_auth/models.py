"""Model de utilizator USV
"""
import os

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.safestring import mark_safe
PATH_IMG='images/'

class UsvUser(models.Model):
    """Contine profilul utilizator USV

    Extinde indirect implementarea standard Django din pachetul
    contrib.auth.models, peste care adauga diferite campuri necesare.
    Nota: extinderea se face folosind o relatie de tip 1:1, in vederea
    extensibilitatii modulului de autentificare si pentru refolosirea lui in
    module de autentificare SSO (OAuth2, etc.)

    Attributes:
        titlu: titlul utilizatorului
        adresa: adresa completa
        telefon: numar telefon
        homepage: adresa web a cadrului didactic
        fotografie: permite vizualizarea planificarilor impreuna cu poza
        is_extern: flag boolean rezervat pentru SSO
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    titlu = models.CharField(max_length=80, blank=True)
    adresa = models.TextField(max_length=1024, blank=True)
    telefon = models.CharField(max_length=40, blank=True)
    homepage = models.URLField(blank=True)
    fotografie = models.ImageField(upload_to=PATH_IMG, blank=True, null=True)
    is_extern = models.BooleanField()

    def foto_url(self):
        """genereaza URL-ul absolut catre imaginea de profil
        """
        if not str(self.fotografie):
            return os.path.join('/', settings.MEDIA_URL, 'usv-sigla2.gif')
        return os.path.join('/', settings.MEDIA_URL, PATH_IMG,
                            os.path.basename(str(self.fotografie)))

    def foto_tag(self):
        """functia creeaza un tag HTML ce contine un thumbnail al imaginii
        uploadate.
        """
        return mark_safe('<img src="{}" width="150" height="150" />'.format(self.foto_url()))

    foto_tag.short_description = 'Imagine profil'

    def __str__(self):
        """functie ajutatoare pentru afisarea obiectelor de tip UsvUser
        """
        return self.user.last_name + ', ' + self.user.first_name

    class Meta:
        ordering = ['user__last_name', 'user__first_name']

    def natural_key(self):
        """Functie Django folosita pentru serializarea obiectelor care contin
        obiecte de tip UsvUser 
        """
        return ({'nume':self.user.get_full_name()})

