# Generated by Django 2.0.5 on 2018-05-16 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usv_structura', '0005_formatie_subgroup_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formatie',
            name='subgroup_id',
        ),
        migrations.AddField(
            model_name='formatie',
            name='subgroup_nume',
            field=models.CharField(default=None, max_length=90, null=True),
        ),
    ]
