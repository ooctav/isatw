from django.forms import ModelForm, ValidationError, ModelChoiceField
from dal import autocomplete

from .models import Formatie, Materie

class MaterieForm(ModelForm):
    """Formularul pentru materii

    Permite adaugarea unor materii noi, editarea lor si relationarea cu modele
    de tip user.
    """

    class Meta:
        model = Materie
        fields = ['nume', 'prescurtare', 'profesori']
        widgets = {
            'profesori':
            autocomplete.ModelSelect2Multiple(url='usv_structura:materii-autocomplete')
        }


class FormatieForm(ModelForm):
    """Formularul pentru formatii de studiu

    Permite adaugarea unor formatii de studiu precizand urmatorii parametrii.

    Attributes:
        nume: numele formatiei de studiu
        tip: tipul formatiei de studiu (facultate, specializare, grupa)
        parinte: nodul parinte (e.g. facultate sau specializare)
    """

    class Meta:
        model = Formatie
        fields = ['nume', 'tip', 'parinte']


    # sortarea se face dupa group_id (parinte sau root), tip si nume ascendent,
    # ceea ce rezulta intr-o structura constanta pentru facultati si
    # specializari, fara a folosi coloane suplimentare pozitionale sau raw SQL
    # queries pentru join, sortare si agregare
    parinte = ModelChoiceField(
        required=False,
        label='Părinte',
        queryset=Formatie.objects.exclude(tip='grupa')
            .order_by('group_id', 'tip', 'nume')
    )
