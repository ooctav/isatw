from django.apps import AppConfig


class UsvFormatiiConfig(AppConfig):
    name = 'usv_structura'
    verbose = 'Componentă formații de studiu'

