import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages

from dal import autocomplete

from .forms import FormatieForm, MaterieForm
from .models import Formatie, Materie

from usv_auth.models import UsvUser

@login_required
@staff_member_required
def index(request, stergere=None):
    """View general pentru formatii

    Formatiile se pot afisa cu toate formularele pe o singura pagina, pentru a
    reduce complexitatea. Se creeaza un singur view, unde in functie de
    parametrii si metoda HTTP, se aleg diferite operatii:
    * listare
    * adaugare
    """

    # sortarea se face dupa group_id (parinte sau root), tip si nume ascendent,
    # ceea ce rezulta intr-o structura constanta pentru facultati si
    # specializari, fara a folosi coloane suplimentare pozitionale sau raw SQL
    # queries pentru join, sortare si agregare
    context = {
        'formatii': Formatie.objects.filter(tip='facultate').order_by('nume')
            # .order_by('group_id', 'subgroup_nume', '-tip', 'nume'),
    }

    if request.method == 'POST':
        form = FormatieForm(request.POST)

        if form.is_valid():
            formatie = form.save()
            formatie.update_group().save()

            messages.add_message(request, messages.INFO,
                'Formație de studiu adăugată cu succes.')
            return redirect('usv_structura:listare')
    else:
        form = FormatieForm()

    context['form'] = form
    return render(request, os.path.join('formatii', 'listare.html'), context)

@login_required
@staff_member_required
def stergere(request, formatie=None):
    """View pentru stergere formatii de studiu 

    Face redirect la pagina de listare.

    Args:
        formatie: id-ul pentru care se doreste stergerea
    """

    formatie = Formatie.objects.get(pk=formatie)
    if formatie.children().count() != 0:
        messages.add_message(request, messages.ERROR,
            'Nu se pot șterge formații de studiu cu sub-elemente. \
            Ștergeți mai întăi sub-elementele și încercați din nou')
        return redirect('usv_structura:listare')
    formatie.delete()
    messages.add_message(request, messages.INFO,
        'Formație de studiu ștearsă cu succes.')

    return redirect('usv_structura:listare')

@login_required
def list_elements(request, parinte=None):
    """Functie ce returneaza specializari sau grupe in functie de parinte.

    Folosit pentru JavaScript asincron, in pagina de listare formatii de
    studiu.

    Args:
        parinte: cheia primara a parintelui, corespunde cu atributul parinte_id
            al copiilor
    """
    context = {
        'formatii' :Formatie.objects.filter(
            parinte_id=parinte
        ).order_by('nume')
    }

    return render(request, os.path.join('formatii', 'listare_js.html'), context)
    return JsonResponse(q, safe=False)


@login_required
@staff_member_required
def materii(request):
    context = {
        'materii': Materie.objects.prefetch_related('profesori')
    }

    if request.method == 'POST':
        form = MaterieForm(request.POST)

        if form.is_valid():
            materie = form.save()

            messages.add_message(request, messages.INFO,
                'Materie adăugată cu succes.')

            return redirect('usv_structura:materii')
    else:
        form = MaterieForm()

    context['form'] = form
    return render(request, os.path.join('materii', 'listare.html'), context)

@login_required
@staff_member_required
def materii_edit(request, materie=None):
    materie_instance = Materie.objects.get(pk=materie)
    context = {}

    if request.method == 'POST':
        form = MaterieForm(request.POST, instance=materie_instance)

        if form.is_valid():
            materie_instance = form.save()

            messages.add_message(request, messages.INFO,
                'Materie modificată cu succes.')

            return redirect('usv_structura:materii')
    else:
        form = MaterieForm(instance=materie_instance)

    context['form'] = form
    context['edit'] = materie is not None
    return render(request, os.path.join('materii', 'editare.html'), context)

@login_required
@staff_member_required
def materii_delete(request, materie=None):
    """View pentru stergere materii

    Face redirect la pagina de listare.

    Args:
        materie: id-ul pentru care se doreste stergerea
    """

    materie = Materie.objects.get(pk=materie)
    materie.delete()
    messages.add_message(request, messages.INFO,
        'Materia a fost ștearsă cu succes.')

    return redirect('usv_structura:materii')

class UsvUserAutocomplete(autocomplete.Select2QuerySetView):
    """Clasa ajutatoare pentru autocomplete ManyToMany

    Cauta obiecte de tip UsvUser, unde relatia User contine textul dat in input
    in campurile first_name si last_name.
    """
    def get_queryset(self):
        qs = UsvUser.objects.all()

        if self.q:
            qs = qs.filter(user__first_name__icontains=self.q) | qs.filter(user__last_name__icontains=self.q)

        return qs
