from django.urls import path
from django.conf.urls import url
from dal import autocomplete

from . import views

app_name = 'usv_structura'

urlpatterns = [
    path('formatii/', views.index, name='listare'),
    path('formatii/stergere/<str:formatie>', views.stergere, name='stergere'),
    path('formatii/listare/<int:parinte>', views.list_elements, name='list_elements'),
    path('materii/', views.materii, name='materii'),
    path('materii/editare/<int:materie>', views.materii_edit, name='editare-materii'),
    path('materii/stergere/<int:materie>', views.materii_delete, name='stergere-materii'),
    url(r'materii/autocomplete/$',
        views.UsvUserAutocomplete.as_view(),
        name='materii-autocomplete'
       ),
]
