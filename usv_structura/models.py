from django.db import models
from django.db.models.functions import Concat
from django.core.exceptions import ValidationError
from usv_auth.models import UsvUser

class Materie(models.Model):
    """Modelul pentru materii

    Avand in vedere ca trebuie sa avem programare de restante, recuperari si
    alte situatii speciale, modelul contine relatii cu structurile de
    profesori (nu si cu formatii de studiu).

    Avantajul este ca materiile si formatiile de studiu sunt independente, si
    pot fi schimbate de la an la an fara a face modificari in materii.

    Attributes:
        nume: numele materiei
        prescurtare: prescurtarea materiei, pentru afisare in calendar
        profesori: relatie de tip many to many catre tabelul de utilizatori
    """
    nume = models.CharField(max_length=90)
    prescurtare = models.CharField(max_length=20)
    profesori = models.ManyToManyField(UsvUser, related_name='materii')

    class Meta:
        ordering = ['nume']

    def __str__(self):
        """Functie de afisare text a obiectului

        Folosita in mare parte la autocomplete. Returneaza corpul si numele
        salii
        """
        return self.nume+ " (" + self.prescurtare+  ")"

    def natural_key(self):
        """Functie Django folosita pentru serializarea obiectelor care contin
        obiecte de tip Materie
        """
        return ({'nume':self.nume, 'prescurtare':self.prescurtare})


class Formatie(models.Model):
    """Contine modelul pentru formatii de studiu

    Modelul foloseste o structura de tip arbore pentru a stoca informatiile,
    care sunt de 3 tipuri (facultate, specializare, grupa). Formatiile copii
    (specializare, grupa) cer obligatoriu un parinte (foreign key catre self).

    Attributes:
        nume: contine numele formatii de studiu
        tip: camp restrictionat la 4 valori (facultate, specializare, grupa)
        parinte: foreign key catre self
        group_id: tine cheia structurii, folosit in ordonare

    TODO: ineficient dpdv spatiu, dar rapid pentru lookups, se poate
    tranzitiona la un model MPTT (modified preorder field traversal)
    """
    FAC, SPE, GRP = 'facultate', 'specializare', 'grupa'
    ALEGERI_FORMATIE = (
        (FAC, 'Facultate'),
        (SPE, 'Specializare'),
        (GRP, 'Grupă'),
    )

    nume = models.CharField(max_length=90, blank=False)
    tip = models.CharField(max_length=12, choices=ALEGERI_FORMATIE, default=FAC)
    parinte = models.ForeignKey('self', blank=True, null=True,
                    on_delete=models.CASCADE, related_name='subformatii')
    group_id = models.IntegerField(default=None, null=True)

    def natural_key(self):
        """Functie Django folosita pentru serializare
        """
        if self.parinte.parinte:
            return ({'tip':self.tip, 'nume':self.parinte.parinte.nume+\
                    " / "+self.parinte.nume+" / "+self.nume})

        if self.parinte:
            return ({'tip':self.tip, 'nume':self.parinte.nume+\
                    " / "+self.nume})

        return self.nume

    def clean(self):
        """Functie ce aplica criteriile custom de validare.

        Daca obiectul nu are parinte, trebuie sa fie de tip facultate.
        Daca obiectul are parinte, trebuie sa fie de tip specializare sau
        grupa.
        Nu exista parinte de tip grupa.
        """

        if self.tip == 'facultate' and self.parinte is not None:
            raise ValidationError({'tip': 'Facultățile nu permit atribut părinte definit'})

        if self.tip == 'specializare':
            # validam facultatea
            fac = None
            if Formatie.objects.filter(pk=self.parinte_id).exists():
                fac = Formatie.objects.get(pk=self.parinte_id)

            if not fac or (fac and fac.tip != 'facultate'):
                raise ValidationError({'tip': 'Specializările trebuie să aibă atribut părinte de tip facultate.'})

        if self.tip == 'grupa':
            # validam specializare 
            spec = None
            if Formatie.objects.filter(pk=self.parinte_id).exists():
                spec = Formatie.objects.get(pk=self.parinte_id)

            if not spec or (spec and spec.tip != 'specializare'):
                raise ValidationError({'tip': 'Grupele trebuie să aibă atribut părinte de tip specializare.'})


    def children(self):
        """Metoda folosita pentru a aduce elementele de tip frunza din cadrul
        unui element superior
        """

        return Formatie.objects.filter(parinte=self.pk)


    def update_group(self):
        """Functie folosita dupa salvarea modelului.

        Daca elementul este parinte (root), seteaza grupul echivalent cu
        primary key. Daca elementul este frunza, seteaza grupul, echivalent cu
        primary key-ul facultatii (root).
        """
        if self.parinte is None:
            self.group_id = self.pk
        else:
            if self.parinte.tip == 'specializare':
                self.group_id = self.parinte.parinte.pk
            else:
                self.group_id = self.parinte.pk

        return self

    def __str__(self):
        """Metoda pentru afisarea obiectului sub forma de string

        Se foloseste la afisarea dropdownului de formatii de studiu. Adauga
        separator (non blank space character, nu spatiu) in fata elementelor
        frunza.
        """
        sep = ''
        if self.tip == 'facultate':
            return self.nume

        if self.tip == 'specializare':
            return self.parinte.nume + " / " + self.nume

        return self.parinte.parinte.nume + " / " + self.parinte.nume +\
                " / " + self.nume

    def anotare():
        """Functie ce creeaza un camp suplimentar, ce afiseaza structura de
        intreaga a formatiei de studiu.

        Foloseste Expression language al ORM-ul Django (forma avansata), si se
        foloseste impreuna cu annotate().
        A durat ceva pana m-am prins :-)

        Examples:
            FIESC / SIC an 1 / Grupa 3711a
            FIESC / SIC an 2
            FIESC
        """
        return models.Case(
            models.When(tip=Formatie.SPE, then=Concat(
                models.F('parinte__nume'),
                models.Value(' / '),
                models.F('nume')
            )),
            models.When(tip=Formatie.GRP, then=Concat(
                models.F('parinte__parinte__nume'),
                models.Value(' / '),
                models.F('parinte__nume'),
                models.Value(' / '),
                models.F('nume')
            )),
            default=models.F('nume')
        )
