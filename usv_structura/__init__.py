"""Modul de structura (materii si formatii de studiu)

In interiorul structurilor USV, avem doua componente majore:
    * ``usv_structura.models.Formatie``: reprezinta o formatie de studiu (an, semigrupa, grupa)

    * ``usv_structura.models.Materie``: materie universitara, impreuna cu profesori asignati

Modulul curent permite editarea, stergerea, listarea si adaugarea structurilor USV in program.

Components:
    * ``usv_structura.models`` : persistenta in baza de date

    * ``usv_structura.urls`` si ``usv_structura.views`` creeaza structura routerului Django, permitand listare, editare, stergere si adaugare de structuri

    * ``usv_structura.forms``: formulare folosite in interactiunea cu utilizatorul, ce permit adaugarea si editarea de structuri

"""
