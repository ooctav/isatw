"""Modul de locatii (sali, laboratoare si birouri)

Defineste obiecte de tip ``usv_loc.models.Sala`` pentru a persista diferite locatii in baza de date. Permite adaugarea de campuri relevante (capacitate, destinatie, nivel, corp, etc.) ce sunt strict legate de infrastructura USV.

Components:
    * ``usv_loc.models`` : persistenta in baza de date

    * ``usv_loc.urls`` si ``usv_loc.views`` creeaza structura routerului Django, permitand listare, editare, stergere si adaugare de Sali

    * ``usv_loc.forms``: formulare folosite in interactiunea cu utilizatorul, ce permit adaugarea si editarea de locatii

"""
