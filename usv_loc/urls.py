from django.urls import path

from . import views

app_name = 'usv_loc'

urlpatterns = [
    path('', views.index, name='listare'),
    path('adaugare', views.adaugare, name='adaugare'),
    path('editare/<str:locatie>', views.adaugare, name='editare'),
    path('stergere/<str:locatie>', views.stergere, name='stergere'),
]
