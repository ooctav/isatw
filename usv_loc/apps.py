from django.apps import AppConfig


class UsvLocConfig(AppConfig):
    name = 'usv_loc'
    verbose = "Componenta locații (săli, laboratoare, birouri, altele"
