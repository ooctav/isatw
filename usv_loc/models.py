from django.db import models

class Sala(models.Model):
    """Contine modelul pentru spatii de lucru

    Permite alegerea intre 4 tipuri de spatii de lucru. Fiecare spatiu are
    obligatoriu un corp si un nivel, reprezentate ca si campuri text. Se poate
    ulterior extinde catre structuri mai complexe (e.g. Corp, Nivel in obiecte
    separate).

    Attributes:
        corp: contine numele corpului
        nivel: contine, in mare parte, etajul
        nume: numerotarea salii
        destinatie: camp text, folosit pentru detalierea scopului
        tip: camp restrictionat la 4 valori (curs, laborator, birou sau altul)
        capacitate: numarul maxim de ocupanti
    """
    CURS, LAB, BIR, AUX = 'curs', 'lab', 'birou', 'aux'
    ALEGERI_LOCATIE  = (
        (CURS, 'Sală curs'),
        (LAB, 'Laborator'),
        (BIR, 'Birou'),
        (AUX, 'Altul'),
    )


    corp = models.CharField(max_length=40, blank=False)
    nivel = models.CharField(max_length=40, blank=False)
    nume = models.CharField(max_length=120, blank=False, unique=True)
    destinatie = models.TextField(max_length=1024, blank=True)
    tip = models.CharField(max_length=5, choices=ALEGERI_LOCATIE, default=LAB)
    capacitate = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        """Functie de afisare text a obiectului

        Folosita in mare parte la autocomplete. Returneaza corpul si numele
        salii
        """
        return self.corp + " / " + self.nume

    def natural_key(self):
        """Serializare JSON
        """
        return ({'corp':self.corp, 'nume': self.nume, 'nivel':self.nivel,
                 'tip':self.tip})
