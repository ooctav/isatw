from django.forms import ModelForm, ValidationError, ChoiceField

from .models import Sala

class ChoiceFieldNoValidation(ChoiceField):
    def validate(self, value):
        pass

class SalaForm(ModelForm):
    """Formularul pentru locatii

    Permite alegerea unor corpuri sau nivele deja existente, sau adaugarea unor
    superlocatii noi; opereaza pe un singur model (si un singur tabel).
    """

    class Meta:
        model = Sala
        fields = ['corp', 'nivel', 'nume', 'tip', 'destinatie', 'capacitate']


    """Constructor formular

    Populeaza campurile corp si nivel cu valorile existente deja in baza de
    date. Functia este asemanatoare unui autocomplete, fara complexitate
    adaugata.

    Tranzitia de la campul de select la input arbitrar (Adaugare **obiect**) se
    face in template, folosind JavaScript.

    Daca formul este bound (are date), si corpul sau nivelul nu corespund
    valorilor reale, adaugam inputul la selectie.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        corpuri = Sala.objects.values_list('corp','corp').distinct()
        nivele = Sala.objects.values_list('nivel','nivel').order_by('nivel').distinct()

        self.fields['corp'].choices = corpuri

        if self.is_bound:
            corp_tuple = (self.data['corp'], self.data['corp'])
            if corp_tuple not in corpuri:
                self.fields['corp'].choices.append(corp_tuple)
        self.fields['corp'].choices.append(('add', 'Adăugare corp'))
        self.fields['nivel'].choices = nivele
        if self.is_bound:
            nivel_tuple = (self.data['nivel'], self.data['nivel'])
            if nivel_tuple not in nivele:
                self.fields['nivel'].choices.append(nivel_tuple)
        self.fields['nivel'].choices.append(('add', 'Adăugare nivel'))

    # rescriem modul de prezentare al campului corp, folosind dropdown
    corp = ChoiceFieldNoValidation(
        label="Corp"
    )

    # rescriem modul de prezentare al campului nivel, folosind dropdown
    nivel = ChoiceFieldNoValidation(
        label="Nivel"
    )
