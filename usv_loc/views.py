import os
from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages

from .forms import SalaForm
from .models import Sala

TEMPLATE_BASE = 'locatii/'

@login_required
@staff_member_required
def index(request):
    """View pentru afisarea locatiilor
    """
    context = {
        'locatii': Sala.objects.order_by('nume').all()
    }

    return render(request, os.path.join(TEMPLATE_BASE, 'listare.html'), context)

@login_required
@staff_member_required
def adaugare(request, locatie=None):
    """View pentru adaugare si editare locatii

    Face redirect la pagina de listare in cazuri de success.

    Args:
        locatie: numele salii pentru care se doreste stergerea
    """
    context = {
        'form': None
    }

    form = None
    sala = None
    if locatie:
        sala = Sala.objects.get(nume=locatie)

    if request.method == 'POST':
        form = SalaForm(request.POST, instance=sala)

        if form.is_valid():
            locatie = form.save()

            messages.add_message(request, messages.INFO,
                'Salvare locație reușită.')
            return redirect('usv_loc:listare')
    else:
        form = SalaForm(instance=sala)

    context['edit'] = locatie is not None
    context['form'] = form
    return render(request, os.path.join(TEMPLATE_BASE, 'adaugare.html'), context)


@login_required
@staff_member_required
def stergere(request, locatie=None):
    """View pentru stergere locatii

    Face redirect la pagina de listare.

    Args:
        locatie: numele salii pentru care se doreste stergerea
    """
    sala = Sala.objects.get(nume=locatie)
    sala.delete()

    messages.add_message(request, messages.INFO,
        'Locație ștearsă cu succes.')

    return redirect('usv_loc:listare')
