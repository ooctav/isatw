# ISATW - Proiect Orar USV

## Instalare

```python
mkvirtualenv --python=/usr/bin/python3 isatw
workon isatw
pip install -r requirements.txt

python manage.py collectstatic --no-input
python manage.py migrate
python manage.py runserver
```

