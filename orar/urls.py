"""orar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
"""
from django.contrib import admin, auth
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from usv_planificare.views.public import index

app_name = 'orar'

urlpatterns = [
    path('', index, name='index'),
    path('profil/', include('usv_auth.urls')),
    path('locatii/', include('usv_loc.urls')),
    path('structura/', include('usv_structura.urls')),
    path('orar/', include('usv_planificare.urls')),
    path('admin/', admin.site.urls),
    path(r'social/', include('social_django.urls')),
    path(r'accounts/', include('django.contrib.auth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

