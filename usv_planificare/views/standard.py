import os
import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, redirect

from usv_planificare.forms import PlanSpecialForm, PlanSpecialAdminForm
from usv_planificare.models import PlanSpecial

def json_planificari(request, mod='profesor', related_pk=None):
    """Functie de afisare (JSON) a planificarilor

    Folosita in toate paginile de afisare, interfatata cu fullcalendar.io

    Args:
        request: obiect de tip request
        mod: cautare filtrata pe un obiect de tip UsvUser, Materie, Formatie
            sau Sala
        related_pk: cheia asociata cu obiectul de mai sus
    Returns:
        Obiect de tip PlanSpecial serializat JSON
    """

    s_input = request.GET.get('start_date')
    if not s_input:
        start_date = datetime.datetime.fromtimestamp(0);
    else:
        start_date = datetime.datetime.fromtimestamp(int(s_input)/1000)

    e_input = request.GET.get('end_date')
    if not e_input:
        end_date = datetime.datetime.fromtimestamp(2147483647)
    else:
        end_date = datetime.datetime.fromtimestamp(int(e_input)/1000)

    if mod == 'profesor':
        if not related_pk and not request.user.is_anonymous:
            related_pk = request.user.usvuser.pk

    qs = json_mod(mod, start_date, end_date, related_pk)

    qs_json = serializers.serialize(
        'json',
        qs,
        use_natural_foreign_keys=True,
        fields=('start_date', 'end_date', 'tip', 'materie',\
                'descriere')
    )
    return HttpResponse(qs_json, content_type='application/json')

def json_mod(mod, start_date, end_date, rel):
    """Functie auxiliara pentru afisare JSON

    Aduce un obiect de tip QuerySet bazat pe PlanSpecial, in functie de
    mod si cheia specificata. Foloseste overloading de tip kwargs pe functia de
    filtru, folosind parametrul ``mod`` si valoarea din ``rel``.

    Args:
        mod: tipul de date cerut [profesor,sala,formatii,materie]
        start_date: datetime pentru limita inferioara
        end_date: datetime pentru limita superioara
        rel: cheia obiectului pe care pivotam

    Returns:
        Django QuerySet folosind managerul din ``mod``
    """
    qs = PlanSpecial.objects

    if mod not in ['profesor', 'sala', 'materie', 'formatii']:
        return qs.none()

    if start_date is not None and end_date is not None:
        if mod == 'formatii':
            qs = (qs.filter(**{mod+'__id': rel}) |\
                 qs.filter(**{mod+'__subformatii__id': rel}) |\
                 qs.filter(**{mod+'__subformatii__subformatii__id': rel})) & \
                    qs.filter(start_date__gte=start_date) & \
                    qs.filter(end_date__lte=end_date)
        else:
            qs = qs.filter(**{mod+'__id': rel}) & \
                    qs.filter(start_date__gte=start_date) & \
                    qs.filter(end_date__lte=end_date)
    else:
        qs = qs.filter(**{mod+'__id':rel})

    return qs.distinct()

@login_required
def index(request):
    """View pentru afisare calendar personal cu planificari de timp

    In cazul in care userul este standard sau administrator, se prezinta un alt
    ecran in care trebuie selectat profesorul (vezi Orar -> Cadre didactice). Daca
    userul este profesor, i se prezinta doar planificarile sale non-recurente.

    """

    return render(request, os.path.join('rezervari', 'listare-special.html'))

@login_required
def rezervare(request, rezervare=None):
    if request.user.is_staff:
        return rezervare_admin(request, rezervare)
    else:
        return rezervare_cadru(request, rezervare)

def rezervare_cadru(request, rezervare):
    """functie rezervare pentru profesori

    Nu lasa selectia campului de tip profesor, foloseste un modelform restrans.
    """
    if rezervare:
        rez_instance = PlanSpecial.objects.get(pk=rezervare)
    else:
        rez_instance = PlanSpecial()

    context = {}

    if request.method == 'POST':
        form = PlanSpecialForm(request.user.usvuser, request.POST, instance=rez_instance)

        if form.is_valid():
            rez_instance = form.save()
            rez_instance.autor = request.user.usvuser
            rez_instance.profesor = request.user.usvuser
            rez_instance.set_notite()
            rez_instance.save()

            messages.add_message(request, messages.INFO,
                'Rezervare salvată cu succes.')

            return redirect('usv_planificare:listare')
    else:
        form = PlanSpecialForm(request.user.usvuser, instance=rez_instance)

    context['form'] = form
    context['edit'] = rezervare is not None
    return render(request, os.path.join('rezervari', 'editare.html'), context)

def rezervare_admin(request, rezervare):
    """functie rezervare pentru administratori

    Permite alegerea tuturor materiilor si a tuturor profesorilor
    """
    if rezervare:
        rez_instance = PlanSpecial.objects.get(pk=rezervare)
    else:
        rez_instance = PlanSpecial()

    context = {}

    if request.method == 'POST':
        form = PlanSpecialAdminForm(request.user.usvuser, request.POST, instance=rez_instance)

        if form.is_valid():
            rez_instance = form.save()
            rez_instance.autor = request.user.usvuser
            rez_instance.set_notite()
            rez_instance.save()

            messages.add_message(request, messages.INFO,
                'Rezervare salvată cu succes.')

            return redirect('usv_planificare:listare')
    else:
        form = PlanSpecialAdminForm(request.user.usvuser, instance=rez_instance)

    context['form'] = form
    context['edit'] = rezervare is not None
    return render(request, os.path.join('rezervari', 'editare.html'), context)

@login_required
def anulare(request, rezervare=None):
    """View pentru stergere materii

    Face redirect la pagina de listare.

    Args:
        materie: id-ul pentru care se doreste stergerea
    """
    rezervare = PlanSpecial.objects.get(pk=rezervare)
    if (not request.user.is_staff) and (request.user.usvuser.pk != rezervare.profesor.pk):
        messages.add_message(request, messages.ERROR,
            'Nu se pot anula rezervări de timp pentru alte cadre didactice.\
                             Contactați un administrator.')
        return redirect('usv_planificare:listare')

    if datetime.datetime.now(datetime.timezone.utc) > rezervare.start_date:
        messages.add_message(request, messages.ERROR,
            'Nu se pot anula rezervări de timp pentru evenimente din trecut.')
        return redirect('usv_planificare:listare')

    rezervare.delete()
    messages.add_message(request, messages.INFO,
            'Rezervarea a fost anulată cu succes.')

    return redirect('usv_planificare:listare')

