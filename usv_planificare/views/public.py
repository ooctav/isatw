# toate view-urile publice
import os

from django.contrib import messages
from django.shortcuts import render, redirect

from usv_planificare.forms import (
    AlegereSalaForm,
    AlegereMaterieForm,
    AlegereFormatieForm,
    AlegereProfesorForm
)

def index(request, tip_obiect=''):
    """View pentru afisare calendar pe sali
    """

    if tip_obiect not in ['profesor', 'sala', 'materie', 'formatii']:
        tip_obiect = 'sala'

    show_calendar = False
    id_obiect = 0

    if request.method == 'POST':
        form = formular_obiect(tip_obiect, request.POST)
        if form.is_valid():
            show_calendar = True
            id_obiect = form[tip_obiect].value
    else:
        form = formular_obiect(tip_obiect)

    context = {
        'form': form,
        'show_calendar': show_calendar,
        'tip_obiect': tip_obiect,
        'id_obiect': id_obiect
    }

    return render(request, os.path.join('rezervari', 'listare-public.html'),
                  context)

def formular_obiect(tip_obiect, post_data=None):
    if tip_obiect == 'sala':
        form = AlegereSalaForm(post_data)
    if tip_obiect == 'materie':
        form = AlegereMaterieForm(post_data)
    if tip_obiect == 'formatii':
        form = AlegereFormatieForm(post_data)
    if tip_obiect == 'profesor':
        form = AlegereProfesorForm(post_data)
    return form
