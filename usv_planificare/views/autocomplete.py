import os

from django.shortcuts import render, redirect
from django.db.models import F

from dal import autocomplete

from usv_structura.models import Materie, Formatie
from usv_loc.models import Sala


class SalaAutocomplete(autocomplete.Select2QuerySetView):
    """Clasa ajutatoare pentru autocomplete ManyToMany

    Cauta obiecte de tip Sala, unde numele sau corpul contin textul dat in input
    """
    def get_queryset(self):
        qs = Sala.objects.all()

        if self.q:
            qs = qs.filter(corp__icontains=self.q) | qs.filter(nume__icontains=self.q)

        return qs

class MaterieAutocomplete(autocomplete.Select2QuerySetView):
    """Clasa ajutatoare pentru autocomplete OneToMany

    Cauta obiecte de tip Materie, unde numele sau prescurtarea contin textul dat in input
    """
    def get_queryset(self):
        staff = self.request.user.is_staff or self.request.user.is_anonymous

        if staff:
            qs = Materie.objects.all()
            if self.q:
                qs = qs.filter(nume__icontains=self.q) | qs.filter(prescurtare__icontains=self.q)
        else:
            qs = Materie.objects.all()
            if self.q:
                qs = qs.filter(profesori__id=self.request.user.usvuser.pk) &\
                    (qs.filter(nume__icontains=self.q) |\
                     qs.filter(prescurtare__icontains=self.q))
            else:
                qs = qs.filter(profesori__id=self.request.user.usvuser.pk)

        return qs

class FormatieAutocomplete(autocomplete.Select2QuerySetView):
    """Clasa ajutatoare pentru autocomplete OneToMany

    Cauta obiecte de tip Formatie, unde numele sau prescurtarea contin textul dat in input
    """
    def get_queryset(self):
        qs = Formatie.objects.annotate(full_path=Formatie.anotare()).all()

        if self.q:
            qs = qs.filter(full_path__icontains=self.q).order_by('full_path')
                # qs.filter(parinte__nume__icontains=self.q) |\
                # qs.filter(nume__icontains=self.q)

        return qs

    def get_result_label(self, obj):
        return obj.full_path
        ret = ""
        if obj.tip == 'specializare':
            ret += obj.parinte.nume + " / "

        if obj.tip == 'grupa':
            ret += obj.parinte.parinte.nume + " / "  + obj.parinte.nume + " / "

        ret += obj.nume

        return ret
