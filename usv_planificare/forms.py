import time
import datetime
from django.utils import timezone
from django.forms import (
    Form,
    ModelForm,
    SplitDateTimeField,
    SplitDateTimeWidget,
    TypedChoiceField,
    ModelChoiceField,
    ValidationError
)
from dal import autocomplete

from .models import PlanSpecial
from usv_auth.models import UsvUser
from usv_loc.models import Sala
from usv_structura.models import Materie, Formatie

class AlegereSalaForm(Form):
    """Formular folosit la alegere sala
    """
    sala = ModelChoiceField(
        label='Sală',
        queryset=Sala.objects,
        widget=autocomplete.ModelSelect2(url='usv_planificare:locatii-ac')
    )

    @property
    def nume(self):
        return  self.cleaned_data.get('sala').corp + " / " + self.cleaned_data.get('sala').nume

    @property
    def obiect(self):
        return self.cleaned_data.get('sala')

class AlegereMaterieForm(Form):
    """Formular folosit la alegere materie
    """
    materie = ModelChoiceField(
        label='Materie',
        queryset=Materie.objects,
        widget=autocomplete.ModelSelect2(url='usv_planificare:materii-ac')
    )

    @property
    def nume(self):
        return str(self.cleaned_data.get('materie'))

    @property
    def obiect(self):
        return self.cleaned_data.get('materie')

class AlegereFormatieForm(Form):
    """Formular folosit la alegere formatii de studiu
    """
    formatii = ModelChoiceField(
        label='Formație de studiu',
        queryset=Formatie.objects,
        widget=autocomplete.ModelSelect2(url='usv_planificare:formatii-ac')
    )

    @property
    def nume(self):
        return str(self.cleaned_data.get('formatii'))

    @property
    def obiect(self):
        return self.cleaned_data.get('formatii')

class AlegereProfesorForm(Form):
    """Formular folosit la alegere cadre didactice
    """
    profesor = ModelChoiceField(
        label='Cadru didactic',
        queryset=UsvUser.objects,
        widget=autocomplete.ModelSelect2(url='usv_planificare:profesori-ac')
    )

    @property
    def nume(self):
        return str(self.cleaned_data.get('profesor'))

    @property
    def obiect(self):
        return self.cleaned_data.get('profesor')

class PlanSpecialForm(ModelForm):
    """Formularul pentru planificari non-recurente (speciale)

    Permite introducerea datelor de catre utilizatori, in vederea stabilirii
    unor planificari de timp, sali si locatii pentru anumite materii.

    Attributes:
        start_date: tip de date compus (data + timp), pentru definirea orei de
            inceput. randarea e ajutata de pickadate.js
        durata: minute pana la finalizarea evenimentului (incremente de 30
            min). nu este mapat la model
    """
    class Meta:
        model = PlanSpecial
        fields = ['tip', 'start_date', 'durata', 'sala', 'materie',
                  'formatii', 'notite']
        widgets = {
            'sala': autocomplete.ModelSelect2(url='usv_planificare:locatii-ac'),
            'materie': autocomplete.ModelSelect2(url='usv_planificare:materii-ac'),
            'formatii': autocomplete.ModelSelect2Multiple(url='usv_planificare:formatii-ac')
        }


    def __init__(self, user, *args, **kwargs):
        """Constructor

        Salveaza userul curent pentru verificare materie
        """
        self.user = user
        super().__init__(*args, **kwargs)

    def get_duration() -> list:
        """Metoda genereaza minute in intervale de 30, de la 0 la 360 (6 ore)

        Generarea implica atat tipul integer cat si labelul (format h:mm)

        Return:
            list of tuples
        """
        return ((x,time.strftime("%H:%M", time.gmtime(x*60))) for x in range(30,390,30))

    start_date = SplitDateTimeField(widget=SplitDateTimeWidget)
    durata = TypedChoiceField(
        label='Durată',
        choices=get_duration(),
        coerce=int
    );

    def clean_start_date(self):
        """Verificare data de inceput

        Daca data este in trecut, nu lasam formularul sa se salveze.
        """
        st_date = self.cleaned_data.get("start_date")

        if st_date < timezone.now():
            raise ValidationError("Evenimentul nu poate fi în trecut!")
        return st_date

    def clean_materie(self):
        """Verificare autorizari

        Daca este cadru didactic, nu lasam sa salveze planificari pentru alte
        materii decat cele asignate.
        """
        mat = self.cleaned_data.get("materie")
        if not self.user.user.is_staff and self.user not in mat.profesori.all():
            raise ValidationError("Materia selectată nu vă aparține.")

        return mat

    def clean_sala(self):
        """Detectie coliziuni

        Verifica daca sala este deja folosita de alta planificare orara. In
        cazul in care se modifica una dintre planificari, se exclude obiectul
        curent
        """

        sala = self.cleaned_data.get("sala")
        start_date = self.cleaned_data.get("start_date")

        if not start_date:
            return sala

        self._set_end_date()
        end_date = self.instance.end_date

        qs = PlanSpecial.objects
        qs = qs.filter(sala__id=sala.pk) &\
            (qs.filter(start_date__range=(start_date, end_date)) |\
            qs.filter(end_date__range=(start_date, end_date)) |\
                ( qs.filter(end_date__gte=end_date) &\
                qs.filter(start_date__lte=start_date) )
            )

        # pentru editare de evenimente, excludem obiectul curent
        if self.instance.pk:
            qs = qs.exclude(id=self.instance.pk)

        if qs.count() > 0:
            raise ValidationError("Sala este deja rezervată pentru alt eveniment.")

        return sala

    def _set_end_date(self):
        """Metoda auxiliara pentru calcul de end_date
        """
        start_date = self.cleaned_data.get("start_date")

        if self.instance:
            self.instance.end_date = start_date +\
                datetime.timedelta(minutes=self.cleaned_data.get("durata")-1);


        return self

    def save(self, commit=True):
        """Salvam modelul dupa validare

        Se extinde timpul de sfarsit din model cu durata stabilita in campul
        ``durata``. Se salveaza modelul.
        """
        self._set_end_date()
        return super().save(commit=commit)



class PlanSpecialAdminForm(PlanSpecialForm):
    """Formularul admin pentru planificari non-recurente (speciale)

    Permite introducerea de rezervari de timp pentru sali, materii, locatii si
    formatii de studiu, pentru orice profesor sau materie.
    """

    class Meta:
        model = PlanSpecial
        fields = ['tip', 'profesor', 'start_date', 'durata', 'sala', 'materie',
                  'formatii', 'notite']
        widgets = {
            'sala': autocomplete.ModelSelect2(url='usv_planificare:locatii-ac'),
            'materie': autocomplete.ModelSelect2(url='usv_planificare:materii-ac'),
            'formatii': autocomplete.ModelSelect2Multiple(url='usv_planificare:formatii-ac'),
            'profesor': autocomplete.ModelSelect2(url='usv_planificare:profesori-ac')
        }

