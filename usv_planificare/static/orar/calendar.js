$(document).ready(function () {
	function nl2br (str, is_xhtml) {
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display

		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}
  // greseala, trebuia facut in Django... meh
  tipuri  = {
    restanta: 'Restanță',
    rec_c: 'Recuperare curs',
    rec_s: 'Recuperare seminar',
    rec_l: 'Recuperare laborator',
    examen: 'Examen',
    alt: 'Altul'
  }
  fetchCalendar = function (start, end, callback) {
    // cerem planificari de la URL specificat
    req = $.ajax({
      method: "get",
      url: calendarUrl+"?start_date="+start+"&end_date="+end,
      cache: false,
    });

    req.done(function (msg) {
      if (Array.isArray(msg))
        parseOrar(msg, callback);
    });

    req.fail(function (jqXHR, textStatus) {
      alert("Eroare citire calendar: " + textStatus);
    });
  }

  parseOrar = function (orarData, callback) {
    events = []
    // luam obiectul returnat de Django si parsam intr-un format inteles de fullcalendar.io
    orarData.forEach(function (el) {
      admin_desc = '<a class="btn btn-sm btn-warning col-6" href="/orar/rezervare/'+el.pk+'">Editare</a>'
      admin_desc += '<a class="btn btn-sm btn-danger col-6" href="/orar/anulare/'+el.pk+'">Ștergere</a><hr>'
      admin_desc += el.fields.descriere
      events.push({
        title: el.fields.materie.prescurtare + " (" +tipuri[el.fields.tip]+")",
        className: "ev-"+el.fields.tip,
        description: (is_admin?admin_desc:el.fields.descriere),
        start: el.fields.start_date,
        end: el.fields.end_date,
        id: el.fields.id
      });
    });

    callback(events);
  }

  // verificam dimensiunea ferestrei, daca e prea mica folosim day agenda
  if ($(window).width() < 500)
    agendaType = 'agendaDay';
  else
    agendaType = 'agendaWeek';

  $('#calendar').fullCalendar({
    weekends: true,
    timezone: "local",
    defaultView: agendaType,
    allDaySlot: false,
    minTime: "07:00:00",
    maxTime: "21:00:00",
    height: 700,
    events: function (start, end, timezone, callback) {
      fetchCalendar(start,end,callback);
    },
    eventRender: function(event, element) {
      element.qtip({
        content: {
          text: nl2br(event.description),
          button: true
        },
        hide: {
          event: false
        },
        show: {
          event: 'click'
        },
        position: {
          my: 'top right',
          at: 'center'
        },
        style: {
          classes: 'qtip-bootstrap qtip-shadow'
        }
      });
    }
    // put your options and callbacks here
  });
});
