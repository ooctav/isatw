from django.urls import path
from django.conf.urls import url

from .views import standard, autocomplete, public
from usv_structura.views import UsvUserAutocomplete

app_name = 'usv_planificare'

urlpatterns = [
    path('rezervare', standard.rezervare, name='rezervare'),
    path('rezervare/<int:rezervare>', standard.rezervare, name='edit-rezervare'),

    path('listare', standard.index, name='listare'),
    path('listare/<str:tip_obiect>', public.index, name='listare-public'),

    path('planificari.json', standard.json_planificari, name='json-planificari'),
    path('planificari/<str:mod>/<int:related_pk>.json', standard.json_planificari, name='json-planificari'),

    path('anulare/<int:rezervare>', standard.anulare, name='anulare'),

    url(r'autocomplete/materii/$',
        autocomplete.MaterieAutocomplete.as_view(),
        name='materii-ac'
       ),
    url(r'autocomplete/locatii/$',
        autocomplete.SalaAutocomplete.as_view(),
        name='locatii-ac'
       ),
    url(r'autocomplete/formatii/$',
        autocomplete.FormatieAutocomplete.as_view(),
        name='formatii-ac'
       ),
    url(r'autocomplete/profesori/$',
        UsvUserAutocomplete.as_view(),
        name='profesori-ac'
       ),
]
