"""Modul de planificare

Defineste componenta principala a aplicatiei. Se pot defini rezervari orare, facand legatura cu modulele ``usv_loc``, ``usv_structura`` si ``usv_auth``. Pe langa functionalitatea de listare, adaugare, editare si stergere planificari, se implementeaza si o functie de detectie a coliziunilor pe sala.

Components:
    * ``usv_planificare.models`` : persistenta in baza de date

    * ``usv_planificare.urls`` si ``usv_planificare.views`` creeaza structura routerului Django, permitand listare, editare, stergere si adaugare de planificari

    * ``usv_planificare.forms``: formulare folosite in interactiunea cu utilizatorul, ce permit adaugarea si editarea de planificari orare

"""
