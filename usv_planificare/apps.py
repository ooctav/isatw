from django.apps import AppConfig


class UsvPlanificareConfig(AppConfig):
    name = 'usv_planificare'
    verbose = 'Componenta USV de planificare timp'
