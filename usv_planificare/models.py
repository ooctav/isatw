from django.db import models
from django.conf import settings

from usv_auth.models import UsvUser
from usv_structura.models import Materie, Formatie
from usv_loc.models import Sala 

class PlanSpecial(models.Model):
    """Clasa pentru planificari orare non-recurente

    Folosita pentru planificari de restante, recuperari, examene.

    Attributes:
        start_date: data de inceput
        end_date: data de sfarsit
        profesor: UsvUser atasat evenimentului
        tip: tip de eveniment, valori multiple
        sala: obiect Sala atasat evenimentului
        materie: obiect Materie atasat evenimentului
        formatii: formatii de studiu atasate evenimentului
        autor: UsvUser care creeaza evenimentul
        notite: camp text pentru comentarii
        descriere = camp text pentru descriere eveniment (si arhivare)
    """
    RES, REC_C, REC_L, REC_S, EXA, ALT = 'restanta', 'rec_c', 'rec_l', 'rec_s', 'examen', 'alt'

    ALEGERI_PLAN_SPEC = (
        (RES, 'Restanță'),
        (REC_C, 'Recuperare curs'),
        (REC_S, 'Recuperare seminar'),
        (REC_L, 'Recuperare laborator'),
        (EXA, 'Examen'),
        (ALT, 'Altul')
    )
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    profesor  = models.ForeignKey(UsvUser, on_delete=models.SET_NULL, null=True)
    autor = models.ForeignKey(UsvUser, on_delete=models.SET_NULL, null=True,
                              related_name='planuri')
    sala = models.ForeignKey(Sala, on_delete=models.SET_NULL, null=True)
    materie = models.ForeignKey(Materie, on_delete=models.SET_NULL, null=True)
    formatii = models.ManyToManyField(Formatie)
    tip = models.CharField(
        max_length=100,
        choices=ALEGERI_PLAN_SPEC,
        default=RES
    )
    notite = models.TextField(max_length=256, blank=True)
    descriere = models.TextField(max_length=4096)

    def set_notite(self):
        """Genereaza descriere

        Folosita la stocarea descrierii, pentru arhivare, in caz ca obiectele
        relationate sunt sterse sau dispar.
        """
        self.descriere = '''- Cadru didactic: {}

- Notite:
{}

- Locatie: {}

- Materie: {}

- Formatii de studiu:
    * {}
'''.format(
    self.profesor.user.get_full_name(),
    self.notite,
    self.sala,
    self.materie,
    "\n   *  ".join(str(o) for o in self.formatii.all())
)
        return self
