# ISATW - Proiect Orar USV

## Echipa:
* Măcărescu Sebastian Traian
* Mihoc Ioana
* Opaschi Octav
* Popeanu Marina
* Stavovei Ciprian

\pagebreak


## Caiet de sarcini

**Client Fictiv S.R.L** dorește o aplicaţie care are în vedere facilitarea procesul rezervării sălilor pentru situații speciale (examene, restanțe, recuperări de laboratoare/seminarii/cursuri, etc), simplificând comunicarea dintre profesori și studenți. De exemplu, cadrul didactic selectează data, ora și sala pentru activitatea propusă.

Studenții vor putea vizualiza întregul orar special pe următoarele cazuri: săli, profesor, materii, formații de studiu. 

În cadrul aplicaţiei se consideră 3 tipuri de utilizatori:

* student (utilizator anonim)
* cadru didactic
* administrator


#### Studentul va avea la dispoziție următoarele opțiuni:

* vizualizarea meniului
* vizualizare orar: 
	- săli
	- cadre didactice
	- materii
	- formații de studiu 

#### Profesorul:

* are posibilitatea de logare folosind infrastructura Google deja existentă în interiorul USV
* poate vizualiza meniul
* poate observa orarul pe următoarele categorii: 
	- săli
	- cadre didactice
	- materii
	- formații de studiu 
* are posibilitatea de alegere a datei, orei, sălii și a materiei pentru situația specială (opțiunile de selecție vor anunța utilizatorul dacă sala este deja ocupată în intervalul orar ales)
	- va putea să anuleze o rezervare
	- va putea să modifice o rezervare 
* poate vizualiza rezervările personale

##### Administratorul:

* logare
* va face actualizările necesare în aplicație pe baza orarului (care a fost actualizat la începutul semestrului de către cadrele didactice)
	- adăugare/editare săli
	- adăugare/editare formații de studiu
	- adăugare/editare materii
	- adăugare/editare cadre didactice
* alegerea profesorului titular + asistent pentru o materie
* adăugare/editare/anulare planificări în numele oricărui cadru didactic
\pagebreak

## Ingineria cerințelor

### Descrierea temei

Proiectul curent are in vedere dezvoltarea unei aplicatii web, in vederea gestionarii orelor in cadrul Universitatii Stefan cel Mare Suceava, cu urmatoarele caracteristici:

* autentificare cadre didactice folosind Google Single Sign On (OAuth2)
* administrare autentificare Google OAuth2 folosind conturi administrative locale (folosind user și parolă) 
* permite adăugarea, ștergerea și editarea planificărilor orare, de către cadrele didactice
* capabilități de detecție coliziuni pentru orice programări în decurs; coliziunile se vor detecta atât pe interval de timp și locație
* posibilitate de notificare email sau SMS a cadrelor didactice în vederea anunțării lor de planificări orare ce urmează într-un interval scurt de timp; funcție ce se implementează ulterior, daca celelalte cerințe sunt deja prezente
* limbaj de programare high-level (python) 
* aplicația se va folosi de tehnologii moderne pentru a implementa cerințele actuale:
	- Stabilirea unui format comun, reutilizabil, pentru afisarea aplicatiei (e.g. Twitter Bootstrap cu diferite componente JavaScript)
	- Mock-up aplicatie
	- Dezvoltare componentă cadre didactice (modificare informații de profil, adăugare și modificare utilizatori); funcții management utilizatori pentru administratori. Diferite nivele de access pentru tipuri de useri.
	- Dezvoltare componentă programări (rezervare, anulare)
	- Dezvoltare componente auxiliare, ce permit relaționarea programărilor cu obiecte de tip materie, sală, formații de studiu, etc.
	- Dezvoltarea unei componente de afișat intervale orare în format calendar; componenta trebuie să fie extensibila, putând fi folosită atat per locatie, cat si global.
	- NU este necesară dezvoltarea unei secțiuni de înregistrare utilizatori (inregistrarea inițială va fi făcută de administratori)
* definire teste aplicație (funcționale, securitate); definire cazuri în care se poate efectua testare automată și cazuri în care nu.
* în toți pașii anteriori, codul va fi însoțit de documentația necesară, incorporată (pentru python – docstrings (PEP257))

### Prezentarea echipei

* Măcărescu Sebastian Traian - Senior Software Developer la ASSIST Software
* Mihoc Ioana - Android developer
* Opaschi Octav - Consultant IT
* Popeanu Marina - Android developer, .NET developer
* Stavovei Ciprian - Web Developer la Infodesign Group


#### Logo-ul echipei MICSO

Echipa MICSO dispune de un logo personalizat ce poate fi observat în **[Figura 1](#fig1)**.

![Logo echipă](figuri/fig1.png){#fig1}

### Analiză SWOT

![Analiză SWOT](figuri/swot.png)\


### Design-ul orientativ al aplicației

Aplicația va suporta cel puțin următoarele opțiuni:

* Afișare program pe săli, cadre didactice, materii sau formații de studiu (**[Figura 2](#fig2)**)
* Rezervare săli (**[Figura 3](#fig3)**)
* Opțiuni administrative de management al locațiilor, cadrelor didactice, a materiilor de studiu sau a formațiilor de studiu (**[Figura 4](#fig4)**)

![Afișare program](figuri/fig2.png){#fig2}

![Rezervare săli](figuri/fig3.png){#fig3}

![Formații de studiu](figuri/fig4.png){#fig4}




### Cerințe hardware și software

Aplicația va fi implementată folosind limbajul de programare [python](https://python.org), împreună cu diferite librării existente sau viitoare. Pentru a putea rula aplicația într-un mediu productiv, următoarele cerințe de sistem sunt recomandate:

* Cerințe hardware: 
	- Minim un procesor 1.2GHz (fizic sau virtualizat)
	- Memorie RAM 512MB
	- Spațiu stocare minim 5GB
	- Rețea conectată la internet sau serviciu de bridging
* Cerințe software:
	- Sistem de operare suportat: Linux
	- Pachete program necesare: python (versiunea 3.4+) și pip (package manager python) instalate
	- Pachete bază de date necesare: sqlite3 (sau, în funcție de baza de date selectată, librăriile aferente)
	- Pentru generare de documentație și generare manual, este recomandată instalarea unui sistem independent cu pachete de dezvoltare (e.g. Sphinx, Pandoc, Texlive-latex, etc.)

În elaborarea proiectului curent, s-au folosit următoarele unelte:

- **[python 3.4](https://python.org)** - limbajul de programare ales
- **[Django framework](https://www.djangoiproject.com)** - framework pentru dezvoltare aplicații web
- Editoare text:
	- **[vim](https://vim.org)**
	- **[Sublime Text 3](https://www.sublimetext.com)**
	- **[Atom](https://atom.io)**
	- **[Visual Studio](https://www.microsoft.com)**
	- **[PyCharm](https://jetbrains.com/pycharm)**
- **[Sphinx](https://sphinx-doc.org)** - unealtă documentare
- **andoc](https://pandoc.org)** - unealtă generare documente în diferite formate (pentru documentare și manual)
- **[Visual Paradigm](https://www.visual-paradigm.com)** - unealtă generare diagrame
- **[Git](https://git-scm.com) și [Bitbucket](https://bitbucket.org)** - pentru dezvoltare și versionare cod sursă
- Alte unelte generice (editoare text, imagine, screenshoturi, etc).

## Diagrama Gantt a proiectului

În vederea elaborării proiectului, s-a ales utilitarul **GanttProject** pentru detalierea etapelor proiectului și bugetul de timp alocat acestora.

Diagrama Gantt, împreună cu alocarea resurselor, este atașată acestui proiect, sub fișierul **[gantt/gantt.pdf](gantt/gantt.pdf)**. De asemenea, ea poate fi vizualizată interactiv folosind fișierul **[gantt/gantt.html](gantt/gantt.html)**
\pagebreak

## Diagrame UML

Pentru descrierea generalizată a aplicației, s-au realizat o serie de diagrame.

### Diagramă de cazuri de utilizare

Având în vedere că aplicația va permite 3 tipuri diferite de access (administrator, cadru didactic și student), următoarele diagrame exemplifică workflow-ul pentru fiecare dintre acestea:

* Diagramă cazuri utilizare student (**[Figura 5](#fig5)**)
* Diagramă cazuri utilizare cadre didactice (**[Figura 6](#fig6)**)
* Diagramă cazuri utilizare administratori (**[Figura 7](#fig7)**)

![Cazuri utilizare student](figuri/diagrama-cazuri-utilizare-student.png){#fig5}

![Cazuri utilizare profesori](figuri/diagrama-cazuri-utilizare-prof.png){#fig6}

![Cazuri utilizare administrator](figuri/diagrama-cazuri-utilizare-admin.png){#fig7}

### Diagramă de componente

Aplicația este compusă din 4 componente majore:

* **usv_auth**: componentă de extindere profile utilizatori Usv
* **usv_loc**: componentă pentru gestiune locații
* **usv_structura**: componentă pentru management materii și formații de studiu
* **usv_planificare**: componentă principală ce interfațează cu toate celelalte trei și permite rezervări de timp

Diagrama pe componente asociată proiectului este prezentată în **[Figura 8](#fig8)**

![Diagramă componente](figuri/diagrama-componente.png){#fig8}

### Diagramă de pachete

Diagrama de pachete reprezintă o generalizare a interacțiunii high-level a sistemului, din prisma frameworkului Django. Un utilizator al aplicației cere **routerului Django** o anumita resursă. Routerul forwardează requestul la **pachetele de randare**, care, în funcție de resursa cerută, instanțiază **pachetul de modele**. Acesta din urmă permite accesul la **baza de date**. Procesul este prezentat în **[Figura 9](#fig9)**.

![Diagramă pachete](figuri/diagrama-pachete.png){#fig9}

### Diagramă de clase

Modelele **Django** asociate structurilor de date conțin următoarele legături:

* Un **UsvUser** (cadru didactic) are o legătură 1:1 cu obiectele implicite **User** din Django.
* O **Formatie** (formație de studiu) are o legătură 1:n cu ea însăși (pentru reprezentarea facultăților, specializărilor și a grupelor)
* O **Materie** conține informații despre o disciplină de studiu, împreuna cu o legătură 1:n cu obiectele de tip **UsvUser** (profesori)
* O **Sala** descrie o locație de studiu sau examen
* Un **PlanSpecial** este modelul de evenimente, și are legături de tip:
	- Many-to-many (n:n) cu **Formatie**
	- Many-to-one (n:1) cu **Sala**, **Materie** și **UsvUser**


Diagrama de clase asociată proiectului este prezentată în **[Figura 10](#fig10)**.

![Diagramă clase](figuri/diagrama-clase.png){#fig10}

\pagebreak

## Documentație generată automat

### Documentație cod sursă
O copie a documentației în format PDF, generată automat, a fost atașată ca anexă documentului curent (**[OrarUSV.pdf](OrarUSV.pdf)**).

Proiectul este acompaniat de documentație generată automat, folosind standardul **[PEP257 - docstrings](https://www.python.org/dev/peps/pep-0257/)** și pachetul **Sphinx**, care se poate instala cu `pip install sphinx`.

Generarea documentației poate fi facută din interiorul folderului de proiect, subdirectorul ``docs``, folosind următoarele comenzi:

Pentru documentație de tip *HTML*:

```sh
cd docs
make html
```

Documentația va fi plasată în folderul `docs/_build/html`.

Pentru documentație de tip *PDF*, se instalează dependințele de sistem (`sudo apt install texlive-latex-recommended texlive-latex-extra latexmk`).

```sh
cd docs
make latex
cd _build/latex
make pdflatex
```

Documentația va fi plasată în folderul `docs/_build/latex/OrarUSV.pdf`. Un exemplu de generare LaTeX este prezent în **[Figura 11](#fig11)**.

![Generare documentație LaTeX](figuri/fig11.png){#fig11}


### Documentație proiect și manual

Restul documentației a fost alcătuită de către echipă și transpusă în format **Markdown**. Acest format poate fi convertit în alte formaturi comune, cum ar fi **docx** sau **pdf**.

Pentru generarea documentației proiectului (documentul curent), precum și pentru manual, se folosește pachetul **pandoc** (`sudo apt install pandoc`).

Generarea se face prin următoarele comenzi:
```
cd docs
./generare_documentatie.sh
```

Outputul programului va conține o arhivă ZIP (`proiect.zip`), cu fișierele `manual.pdf`, `manual.docx`, `proiect.pdf` și `proiect.docx`, precum și diagramele Gantt în format HTML și o copie a documentației cod sursă generată automat.

Manualul de utilizare este atașat proiectului curent (**[manual.pdf](manual.pdf)**).

\pagebreak

## Testarea proiectului

Pentru a valida funcționalitatea aplicației create, s-au făcut o serie de teste funcționale și de securitate, manual.

S-a ales folosirea de teste tip "User Story" în creearea unor cazuri de test:

|Componenta|Story line			|Rezultat așteptat|Rezultat actual|
|----------|:------------------------------|----------------:|--------------:|
|Orar|Ca user anonim, pot selecta materii pentru a vizualiza programări pentru acestea|Da|Da|
|Orar|Ca user anonim, pot selecta săli pentru a vizualiza programări pentru acestea|Da|Da|
|Orar|Ca user anonim, pot selecta cadre didactice pentru a vizualiza programări pentru acestea|Da|Da|
|Orar|Ca user anonim, pot selecta formații de studiu pentru a vizualiza programări pentru acestea|Da|Da|
|Orar|Ca user anonim, pot accesa pagina de login pentru a mă autentifica|Da|Da|
|Orar|Ca user anonim, pot naviga paginile cu calendar pentru a vedea programările altor săptămâni|Da|Da|
|Google OAuth2|Ca user anonim, pot apăsa pe butonul Autentificare cu Google pentru a fi redirecționat la o pagină Google|Da|Da|
|Login|Ca user anonim, pot introduce datele inițiale de autentificare pentru a fi redirecționat la o pagină de administrare|Da|Da|
|Funcții administrative|Ca administrator, pot accesa toate elementele meniului FUNCȚII ADMINISTRATIVE pentru a afișa paginile administrative |Da|Da|
|Cadre didactice|Ca administrator, pot accesa itemul Adăugare Cadre Didactice pentru a adăuga un nou cadru didactic|Da|Da|
|Cadre didactice|Ca administrator, nu pot adăuga cadre didactice cand lipsesc câmpuri obligatorii (nume utilizator, email, prenume, nume)|Da|Da|
|Cadre didactice|Ca administrator, nu pot adăuga cadre didactice cand adresa de mail sau utilizatorul sunt deja folosite|Da|Da|
|Cadre didactice|Ca administrator, pot specifica și alte câmpuri opționale când adaug cadre didactice|Da|Da|
|Cadre didactice|Ca administrator, pot specifica Stare autorizare pentru a adăuga cadre didactice cu permisiuni administrative|Da|Da|
|Cadre didactice|Ca administrator, pot vizualiza lista utilizatorilor deja înregistrați|Da|Da|
|Structură facultăți|Ca administrator, pot vizualiza lista formațiilor de studiu|Da|Da|
|Structură facultăți|Ca administrator, pot expanda lista facultăților in subcomponente|Da|Da|
|Structură facultăți|Ca administrator, pot șterge formații de studiu care nu au subelemente|Da|Da|
|Structură facultăți|Ca administrator, nu pot șterge formații de studiu care au subelemente|Da|Da|
|Structură facultăți|Ca administrator, pot adăuga formații de studiu|Da|Da|
|Structură facultăți|Ca administrator, nu pot adăuga specializări fără a selecta un părinte de tip facultate|Da|Da|
|Structură facultăți|Ca administrator, nu pot adăuga grupe fără a selecta un părinte de tip specializare|Da|Da|
|Structură facultăți|Ca administrator, nu pot adăuga facultăți dacă aleg un tip|Da|Da|
|Structură facultăți|Ca administrator, nu pot adăuga formații de studiu fără nume|Da|Da|
|Structură facultăți|Ca administrator, pot apăsa pe pagina Materii pentru a afișa lista materiilor|Da|Da|
|Structură facultăți|Ca administrator, pot apăsa butonul de ștergere din dreptul fiecărui element din listă pentru a șterge materii|Da|Da|
|Structură facultăți|Ca administrator, nu pot adăuga materii dacă nu specific toate 3 câmpurile|Da|Da|
|Structură facultăți|Ca administrator, pot scrie nume parțiale pentru a căuta profesori în lista de materii|Da|Da|
|Structură facultăți|Ca administrator, pot adăuga mai mulți profesori pentru o singură materie|Da|Da|
|Structură facultăți|Ca administrator, pot adăuga materii pentru a le vedea în lista de materii|Da|Da|
|Management locații|Ca administrator, pot apăsa butonul listare pentru a vedea lista de Management Locații|Da|Da|
|Management locații|Ca administrator, pot apăsa butonul adăugare pentru a vedea pagina de adăugare săli|Da|Da|
|Management locații|Ca administrator, nu pot adăuga săli dacă nu specific numele|Da|Da|
|Management locații|Ca administrator, nu pot adăuga săli dacă specific nume deja existent|Da|Da|
|Management locații|Ca administrator, pot adăuga săli dacă specific capacitate 0|Da|Da|
|Management locații|Ca administrator, pot adăuga săli dacă nu specific destinație sală|Da|Da|
|Planificare|Ca administrator, pot apăsa butonul Rezervare sală pentru a vedea pagina de rezervare|Da|Da|
|Planificare|Ca administrator, nu pot rezerva sală daca nu introduc profesor, data de start, durată, sală, materie sau formații|Da|Da|
|Planificare|Ca administrator, pot scrie părți din nume pentru a selecta profesor|Da|Da|
|Planificare|Ca administrator, pot scrie părți din nume pentru a selecta materie|Da|Da|
|Planificare|Ca administrator, pot scrie părți din nume pentru a selecta o sală|Da|Da|
|Planificare|Ca administrator, pot scrie părți din nume pentru a selecta o formație de studiu|Da|Da|
|Planificare|Ca administrator, pot adăuga mai multe formații de studiu|Da|Da|
|Planificare|Ca administrator, sunt atenționat de coliziuni pe sală când selectez un interval orar deja ocupat|Da|Da|
|Orar|Ca administrator, pot folosi orice funcție anonimă pentru a verifica planificările|Da|Da|
|Login|Ca și cadru didactic, pot accepta permisiunile de citire de la Google pentru a fi redirecționat la o pagină înregistrată|Da|Da|
|Orar|Ca și cadru didactic, pot folosi orice funcție anonimă pentru a verifica planificările|Da|Da|
|Planificare|Ca și cadru didactic, pot apăsa butonul Vizualizare program pentru a vedea lista mea de planificări|Da|Da|
|Planificare|Ca și cadru didactic, pot apăsa butonul Rezervare sală pentru a vedea pagina de rezervare|Da|Da|
|Planificare|Ca și cadru didactic, nu pot rezerva sală daca nu introduc profesor, data de start, durată, sală, materie sau formații|Da|Da|
|Planificare|Ca și cadru didactic, nu văd nici un câmp pentru selecție profesor|Da|Da|
|Planificare|Ca și cadru didactic, pot scrie părți din nume pentru a selecta materie|Da|Da|
|Planificare|Ca și cadru didactic, nu pot selecta sau vedea materii ce nu îmi sunt asignate|Da|Da|
|Planificare|Ca și cadru didactic, pot scrie părți din nume pentru a selecta o sală|Da|Da|
|Planificare|Ca și cadru didactic, pot scrie părți din nume pentru a selecta o formație de studiu|Da|Da|
|Planificare|Ca și cadru didactic, pot adăuga mai multe formații de studiu|Da|Da|
|Planificare|Ca și cadru didactic, sunt atenționat de coliziuni pe sală când selectez un interval orar deja ocupat|Da|Da|


\pagebreak

## Mentenanță, contact, training și issue tracking


### Mentenanță

În vederea stabilirii unor proceduri de mentenanță, se dau următoarele caracteristici:

* Mentenanța se va face semestrial, în functie de cerințele clientului. Data de release pentru orice versiune de mentenanța este prima Joi (lucrătoare) a fiecărui semestru. Alternativ, se pot releasui versiuni noi în următoarea joi.
* În cazul unor corecții de securitate, intervalul de mentenanță nu va fi respectat. Releasurile de securitate au prioritate maximă, fiind released în momentul în care se repară defectele.
* Clientul are dreptul la 5 cereri de modificări minore pe parcursul unei perioade de un an.
* Dacă se doresc mai mult de 5 modificări minore, sau este necesară adăugarea de functionalitate noua, clientului îi va fi înaintată o propunere pentru dezvoltare suplimentară.
* Orice defect software, ce nu corespunde specificațiilor acestui document sau a manualului acompaniat va fi reparată gratuit, în cadrul procedurii de mentenanță.

### Contact

Contactul principal al clientului se face cu ajutorul platformei email, la managerul de proiect.

### Training

Pentru proceduri de training în folosirea aplicației sau pentru prezentări de tip awareness training, se percepe un tarif suplimentar de 500EUR pe 4h. Orice interval de training trebuie sa fie multiplu de 4h.

### Issue tracking

Pentru issue tracking, se folosește platforma Jira (intern). Procesul poate fi ușor integrat cu repository-ul extern **Bitbucket.org**.

În timpul testării din capitolele anterioare, s-a descoperit următorul bug:

Dacă se încerca rezervarea unui interval de timp cuprins (în interiorul) unui alte planificări deja existente, detecția de coliziuni nu funcționa și lăsa planificarea să fie salvată. Problema a fost identificată în fișierul `usv_planificare/forms.py`, mai exact în funcția de detecție coliziuni `clean_sala()`:

```python
         qs = qs.filter(sala__id=sala.pk) &\
             (qs.filter(start_date__range=(start_date, end_date)) |\
             qs.filter(end_date__range=(start_date, end_date)))
```

După cum se observă, unul dintre cazurile de comparație a fost omis. Rezolvarea a fost aplicată prin schimbarea comparației în:

```python
         qs = qs.filter(sala__id=sala.pk) &\
             (qs.filter(start_date__range=(start_date, end_date)) |\
             qs.filter(end_date__range=(start_date, end_date)) |\
                 ( qs.filter(end_date__gte=end_date) &\
                 qs.filter(start_date__lte=start_date) )
             )
```

Un exemplu al problemei se poate regăsi în **[Figura 12](#fig12)**

![Detecție coliziuni nefuncțională](figuri/bug1.png){#fig12}
