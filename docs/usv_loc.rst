usv\_loc package
================

Submodules
----------

usv\_loc.admin module
---------------------

.. automodule:: usv_loc.admin
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.apps module
--------------------

.. automodule:: usv_loc.apps
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.forms module
---------------------

.. automodule:: usv_loc.forms
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.models module
----------------------

.. automodule:: usv_loc.models
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.tests module
---------------------

.. automodule:: usv_loc.tests
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.urls module
--------------------

.. automodule:: usv_loc.urls
    :members:
    :undoc-members:
    :show-inheritance:

usv\_loc.views module
---------------------

.. automodule:: usv_loc.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: usv_loc
    :members:
    :undoc-members:
    :show-inheritance:
