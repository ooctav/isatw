usv\_planificare.views package
==============================

Submodules
----------

usv\_planificare.views.autocomplete module
------------------------------------------

.. automodule:: usv_planificare.views.autocomplete
    :members:
    :undoc-members:
    :show-inheritance:

usv\_planificare.views.public module
------------------------------------

.. automodule:: usv_planificare.views.public
    :members:
    :undoc-members:
    :show-inheritance:

usv\_planificare.views.standard module
--------------------------------------

.. automodule:: usv_planificare.views.standard
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: usv_planificare.views
    :members:
    :undoc-members:
    :show-inheritance:
