# ISATW - Proiect Orar USV

## Echipa:
* Măcărescu Sebastian Traian
* Mihoc Ioana
* Opaschi Octav
* Popeanu Marina
* Stavovei Ciprian

\pagebreak


## Instalare și configurație

### Instalare aplicație

Pentru instalarea aplicației este necesară o mașină (fizică sau virtuală) cu cel puțin 1.2GHz procesor și 512MB RAM. Este recomandată folosirea unei distribuții Linux bazate pe Debian (e.g. Ubuntu). Procesul de instalare presupune existența unor pachete deja instalate pe sistemul țintă. Instalarea pachetelor se poate face prin comenzile:

```sh
sudo apt install virtualenvwrapper python3 libsqlite3 python3-virtualenv
```

După instalarea pachetelor, se poate creea un environment de tip *Python3*, folosind aplicația `mkvirtualenv`. În medii productive, se poate alege instalarea acestor pachete direct pe o mașină virtuală sau pe un sistem containerizat, caz în care se poate opera fără `virtualenv`. Instalarea mediului de lucru python se face cu:

```sh
mkvirtualenv --python=/usr/bin/python3 isatw
workon isatw
```

Se clonează repository-ul de cod în directorul curent, folosind următoarea comandă: `git clone https://bitbucket.org/ooctav/isatw.git`.

După instanțierea mediului de lucru și a surselor aplicației, se pot instala dependințele necesare unui deployment. Acestea sunt stocate într-un fișier `requirements.txt` aflat în rădăcina directorului sursă.

```sh
cd isatw
pip install -r requirements.txt
```

Se instațiază modulul Django și paginile de tip static și baza de date, folosind următoarele comenzi:
```sh
python manage.py collectstatic --no-input
python manage.py migrate
python manage.py createsuperuser # pentru a crea un cont superuser (initial)
```


### Configurare aplicație

Configurația aplicației poate fi modificată de către client, folosind fișierul de configurare `orar/settings.py`. Următorul tabel prezintă diferitele setări de configurare ce sunt recomandate pentru deployment:

-------------------------------------------------------------------------------------------------------------------------
Setare                                       Valoare                                     Descriere                                 
-------------------------------------------  ------------                                -------------------------------------
TIME\_ZONE                                   EET                                         Zona de timp folosită în localizare (limbă) și date. Dacă se dorește folosirea în alte zone geografice, se recomandă schimbarea valorii

SOCIAL\_AUTH\_ GOOGLE\_OAUTH2\_ KEY          Cheia autentificării cu Google SSO          Pentru detalii și metode de obținere a cheii, a se vedea secțiunea **[Google OAuth2](#google-oauth2)**

SOCIAL\_AUTH\_ GOOGLE\_OAUTH2\_ SECRET       Secretul autentificării cu Google SSO       Pentru detalii și metode de obținere a secretului, a se vedea secțiunea **[Google OAuth2](#google-oauth2)**

DEBUG                                        True                                        Dacă se dorește ascunderea mesajelor de eroare detaliate, este recomandată setarea valorii pe „False”

SECRET\_KEY                                  String random                               Se generează un string random și se introduce în acest câmp pentru criptarea sesiunilor și a datelor cu caracter privat

ALLOWED\_HOSTS                               Listă cu adrese IP sau hostnames            În această listă sunt specificate toate domeniile de pe care se poate accesa situl. Dacă se dorește instalarea pe alte domenii, trebuie adăugate în listă

--------------------------------------       ------------                                -------------------------------------


### Lansarea aplicației

Aplicația poate fi lansată cu comanda `python manage.py runserver`. Alternativ, se poate folosi orice server compatibil WSGI (e.g. Google Cloud Engine, Heroku, gunicorn, etc.).

\pagebreak

## Utilizare aplicație

Capitolul curent tratează metodele de folosire ale aplicației și descrie diferitele elemente structurale și de design.

### Structură generală, meniuri

Aplicația este destinată planificării orare a unor situații speciale, în cadrul **USV**, cum ar fi: recuperare cursuri, recuperare laboratoare, recuperare seminarii, examene și restanțe. Pentru a permite aceste operații, au fost implementate 3 tipuri diferite de utilizatori:

* Administratori: permit adăugarea de cadre didactice, management locații și alte operații cu caracter administrativ.
* Cadre didactice: pot rezerva anumite intervale de timp pentru desfășurarea activităților speciale.
* Studenți: useri anonimi, pot vizualiza planificările orare pe mai multe criterii.

Structura generală a aplicației include un meniu lateral, vizibil doar pe dispozitive cu rezolutie mare, putând fi activat explicit și pe dispozitive mobile. Meniul lateral permite navigarea cu ușurință a aplicației, pentru toate categoriile de utilizatori. De asemenea, conținutul meniului variază pentru fiecare tip de user, în funcție de nivelul de autorizare.

Partea superioară a ecranului conține o bandă informativă (header), și un buton pentru autentificare, cu scopul de a autentifica utilizatorii (cadre didactice și administratori). Autentificarea se poate face cu user și parolă (daca utilizatorul a fost adăugat din consola python), sau cu Google SSO (OAuth2).

**[Figura 1](#fig1)** prezintă ecranul de autentificare al aplicației, headerul, precum si funcția de login.

![Pagină login](figuri/fig1.png){#fig1}

\pagebreak

### Funcții administrative

Utilizatorii administrativi au acces la funcții ce le permit să adauge, modifice și sterge următoarele tipuri de obiecte:

* Cadre didactice
* Săli și locații
* Materii de studiu
* Formații de studiu

Obiectele sus menționate sunt folosite oricând un cadru didactic sau administrator face o rezervare / planificare de timp.

#### Cadre didactice

În meniul de cadre didactice, se pot executa următoarele acțiuni:

* Listare cadre didactice
* Adăugare/editare cadru didactic
* Ștergere cadru didactic

Un cadru didactic poate fi mai apoi folosit în obiecte de tip materie, în rezervări sau vizualizare planificări.

Listarea cadrelor didactice se face apăsând butonul „Listare” din meniul corespondent. Tabelul conține toți utilizatorii aplicației, exemplificat în **[Figura 2](#fig2)**. Folosind acțiunile din dreapta listei de cadre didactice, se pot iniția și alte acțiuni cum ar fi editare sau ștergere de cadre didactice.

![Listare cadre didactice](figuri/fig2.png){#fig2}

Pentru operațiile de adăugare sau editare, următorii parametrii sunt obligatorii:

- Nume utilizator: trebuie să fie unic
- Adresă email: trebuie să fie unică; câmp absolut necesar pentru autentificare cu Google SSO. Dacă utilizatorul nu are cont Google, nu va putea folosi aplicația ca și cadru didactic.
- Nume și prenume: folosite în funcțiile de afișare

Alte câmpuri, care nu sunt obligatorii, sunt:

- Stare autorizare: când este activat, userul devine administrator, având acces la funcțiile de din capitolul curent.
- Titlu: titlul academic, dacă este cerut și există
- Adresa, telefon, homepage și fotografie: câmpuri opționale, folosite la afișare de useri și la căutare după cadre didactice

Un exemplu de pagină de adăugare și editare este prezent în **[Figura 3](#fig3)**.

![Editare utilizator](figuri/fig3.png){#fig3}

\pagebreak

#### Management locații

Secțiunea „Management locații” permite adăugarea de săli de curs, laborator, seminar și birouri pentru a fi folosite în planificarea evenimentelor speciale. Sunt posibile următoarele acțiuni:

* Listare locații
* Adăugare/modificare locații
* Ștergere locații

Listarea locațiilor se face apăsând butonul „Listare” din meniul corespunzător. Tabelul conține toate locațiile deja definite și alte detalii despre ele. Folosind acțiunile din dreapta listei, se pot iniția și alte operațiuni cum ar fi editare sau ștergere. Listarea este exemplificată în **[Figura 4](#fig4)**.

![Listare locații](figuri/fig4.png){#fig4}

Pentru adăugare sau editare locații, sunt prezenți următorii parametrii:

* Corp: poate fi adăugat unul nou, prin selecția opțiunii „Adăugare corp”, sau poate fi ales unul deja existent. Parametru obligatoriu.
* Nivel: poate fi adăugat unul nou, prin selecția opțiunii „Adăugare nivel”, sau poate fi ales unul deja existent. Parametru obligatoriu.
* Nume: numerotarea sălii. În general, se folosește formatul „<inițială corp> <număr sală>”. Câmp obligatoriu.
* Tip: permite alegerea unui tip de sală: Laborator, Curs, Birou sau Altul.
* Destinație: parametru opțional ce permite adăugarea unor detalii ale sălii.
* Capacitate: număr de persoane suportat. 

Pagina de adăugare și editare este exemplificată în **[Figura 5](#fig5)**.

![Editare locație](figuri/fig5.png){#fig5}

\pagebreak

#### Materii

Secțiunea administrativă materii permite listarea și adăugarea materiilor de studiu. Unei materii îi sunt asociate:

- un nume (e.g. Programare Paralelă)
- o prescurtare (e.g. PP), folosită în listarea ei în calendar
- unul sau mai multe cadre didactice; câmp obligatoriu - **doar cadrele didactice asignate unei materii pot face rezervări pentru această materie** (mai puțin administratorii, care pot planifica orice materie).

Listarea, editarea și ștergerea materiilor de studiu se face tot din această secțiune. Un exemplu este prezentat în **[Figura 6](#fig6)**.

![Pagină materii de studiu](figuri/fig6.png){#fig6}

\pagebreak

#### Formații de studiu

Formațiile de studiu reprezintă facultățile, specializările și grupele pentru care se fac planificări. Pentru fiecare formație de studiu se cer:

- un nume, obligatoriu (e.g. FIESC)
- un tip, obligatoriu, una dintre următoarele valori: facultate, specializare sau grupă
- un părinte, dacă tipul susmenționat este specializare sau grupă

Pe aceeași pagină se pot lista formațiile de studiu tip facultate, cu posibilitate ulterioară de expandare a alegerii și pe specializări și grupe. Ca acțiuni, se pot doar șterge formații de studiu, care nu au nici un subelement (pentru a șterge o specializare sau facultate completă, trebuie să fie șterse toate sub-componentele, mai întâi.

Funcționalitatea de formații de studiu este prezentată în **[Figura 7](#fig7)**.

![Formații de studiu](figuri/fig7.png){#fig7}

\pagebreak

### Funcții cadre didactice

Această secțiune prezintă funcționalitatea disponibilă atât utilizatorilor de tip administrator și celor de tip cadre didactice. Funcționalitatea permite alegerea anumitor săli, cadre didactice, materii și formații de studiu în vederea rezervării unor intervale orare.

#### Rezervare

Funcția de rezervare poate fi accesată apăsând butonul aferent din meniu („Rezervare sală”). Odată afișată, se cer următorii parametri:

- Tipul planificării: alegere între restanța, diferite tipuri de recuperări, examen sau altul. În funcție de alegerea de tip, în calendarul de vizualizare vor fi afișate diferite culori de fundal în dreptul evenimentului.
- Profesor: cadrul didactic desemnat acestei rezervări; opțiune afișată doar administratorilor. În cazul utilizatorului de tip cadru didactic simplu, opțiunea lipsește.
- Start date: două câmpuri de selecție, unu pentru data de început și ora de început (notă: calendarul afișează evenimente doar în intervalul 07:00 - 21:00). Câmp obligatoriu
- Durată: durata evenimentului, câmp obligatoriu.
- Sala: alegerea sălii desemnate pentru eveniment, câmp obligatoriu.
- Materie: câmp obligatoriu. În cazul cadrelor didactice, **acestea pot selecta doar materii la care au fost desemnate de către administratori**.
- Formații: una sau mai multe formații de studiu; se pot alege și „superformații” de tip facultate
- Notițe: comentarii sau note adiționale ce vor fi afișate împreună cu evenimentul.

Un exemplu de adăugare sau editare planificare temporară este vizibil în **[Figura 8](#fig8)**.

![Editare planificare](figuri/fig8.png){#fig8}

\pagebreak

### Vizualizare planificări

Vizualizarea planificărilor este parte din funcțiile publice, ce permit afișarea programărilor făcute de către administratori și cadre didactice tuturor vizitatorilor siteului. Ele sunt structurate în 4 categorii:

* Pe săli
* Pe cadre didactice
* Pe materii
* Pe formații de studiu (inclusiv sub-formații)

Toate funcțiile permit, după alegerea unui element de pivot (sală, cadru didactic, materie, formație de studiu), afișarea unui calendar cu evenimente corespunzătoare săptamânii curente. Utilizatorul poate naviga cu ușurință și alte săptămâni (atât în trecut cât și în viitor). Un exemplu de programare, pe sală (pivot C 001), este prezent în **[Figura 9](#fig9)**. Dacă se apasă (cu mouse sau touch), pe oricare dintre planificări, se observă detaliile aferente evenimentului.

![Selecție după sală](figuri/fig9.png){#fig9}

În cazul în care utilizatorul este cadru didactic sau administrator, sunt afișate două butoane ce permit editarea și ștergerea evenimentelor (dacă acestea nu sunt în trecut, caz în care nu se mai pot modifica - echivalen funcție de arhivare).

\pagebreak

## Google OAuth2

Secțiunea curentă prezintă pașii necesari configurației Google Single Sign-On, folosind OAuth2.

Pentru autentificare folosind Google Single Sign-On (folosind OAuth2), trebuie definită o aplicație în **Google Cloud Dashboard**. Pașii necesari sunt următorii:

1. Se navighează la [Google Cloud Dashboard](https://console.cloud.google.com/) folosind contul de *student@student.usv.ro*. (**[Figura 10](#fig10)**)
2. Se creeaza un proiect nou, in cazul in care nu se poate folosi un proiect deja existent. Numele proiectului poate fi ales aleator. (**[Figura 11](#fig11)**)

![Creeare proiect](figuri/google-sso/proiect.PNG){#fig10}
![Creeare proiect 2](figuri/google-sso/proiect2.png){#fig11}

3. În cadrul noului proiect, folosind meniul lateral, se selectează **API & Services** și se alege **Library**. În fereastra nou deschisă se caută *Google+ API* și se activează. (**[Figura 12](#fig12)**)

![Activare Google+ API](figuri/google-sso/proiect3.png){#fig12}

4. După activarea API-ului de *Google+*, se selectează, din submeniul lateral, secțiunea **Credentials**, după care se intră în tabul *OAuth consent screen*, unde trebuie introdus numele proiectului arătat userilor când li se cere permisiunea, din partea Google. Se recomandă alegerea unui nume descriptiv (e.g. Orar USV). (**[Figura 13](#fig13)**)

![Setari de consimțământ](figuri/google-sso/consent.png){#fig13}

5. Se selectează, din submeniul lateral, secțiunea **Credentials**, după care se apasă butoanele *Create credentials*, *OAuth client id*. În configurația clientului de *OAuth2*, se introduc următoarele date:

* Application type: *Web application*
* Name: *ISATW1*
* Restrictions:
	- Authorized redirect URIs: *http(s)://cale-catre-aplicatie/social/complete/google-oauth2/*

![Setari OAuth2](figuri/google-sso/config.png)

După salvare, se copiază în configuratia aplicației, situată în locația `orar/settings.py`, secretul si id-ul clientului de OAuth2.
