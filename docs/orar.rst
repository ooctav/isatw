orar package
============

Submodules
----------

orar.settings module
--------------------

.. automodule:: orar.settings
    :members:
    :undoc-members:
    :show-inheritance:

orar.urls module
----------------

.. automodule:: orar.urls
    :members:
    :undoc-members:
    :show-inheritance:

orar.wsgi module
----------------

.. automodule:: orar.wsgi
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: orar
    :members:
    :undoc-members:
    :show-inheritance:
