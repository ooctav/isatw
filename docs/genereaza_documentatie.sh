#!/bin/sh


mkdir out
make latex
cd _build/latex
make
cp OrarUSV.pdf ../../out/
cd ..
cd ..
cd _manual
pandoc manual.md -o ../out/manual.pdf
pandoc manual.md -o ../out/manual.docx

cd ..
cd _proiect
pandoc proiect.md -o ../out/proiect.pdf
pandoc proiect.md -o ../out/proiect.docx

cd ..
cp -a _proiect/gantt/ out/
zip -r proiect.zip out

make clean
rm -rf out


