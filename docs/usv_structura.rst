usv\_structura package
======================

Submodules
----------

usv\_structura.admin module
---------------------------

.. automodule:: usv_structura.admin
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.apps module
--------------------------

.. automodule:: usv_structura.apps
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.forms module
---------------------------

.. automodule:: usv_structura.forms
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.models module
----------------------------

.. automodule:: usv_structura.models
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.tests module
---------------------------

.. automodule:: usv_structura.tests
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.urls module
--------------------------

.. automodule:: usv_structura.urls
    :members:
    :undoc-members:
    :show-inheritance:

usv\_structura.views module
---------------------------

.. automodule:: usv_structura.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: usv_structura
    :members:
    :undoc-members:
    :show-inheritance:
